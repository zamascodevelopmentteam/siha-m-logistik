/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.hipechat.util.extension

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.text.Spanned
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import hi.studio.msiha.util.HashUtils
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import kotlin.math.log10
import kotlin.math.pow



/**
 * Created by mochadwi on 2019-10-30
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

const val httpHeaderBearerTokenPrefix = "Bearer"

// TODO(mochadwi): 2019-10-24 Optimize it using Kotlinx Serialization
val gson: Gson by lazy { Gson() }

inline fun <reified T> T.toJson(): String = gson.toJson(this)

inline fun <reified T> String.fromJson(): T =
    gson.fromJson(this, T::class.java)

fun getReadableFileSize(size: Long): String {
    if (size <= 0) return "0"
    val units = arrayOf("B", "kB", "MB", "GB", "TB")
    val digitGroups = (log10(size.toDouble()) / log10(1024.0)).toInt()
    return DecimalFormat("#,##0.#").format(
        size / 1024.0.pow(digitGroups.toDouble())
    ) + " " + units[digitGroups]

}

fun String.sha512() = HashUtils.hashString("SHA-512", this)

fun String.sha256() = HashUtils.hashString("SHA-256", this)

fun String.sha1() = HashUtils.hashString("SHA-1", this)

fun String.hasBearerAuthTokenPrefix(): Boolean {
    return this.startsWith(httpHeaderBearerTokenPrefix)
}

fun String.withBearerAuthTokenPrefix(): String {
    return when (this.hasBearerAuthTokenPrefix()) {
        true -> this
        false -> "$httpHeaderBearerTokenPrefix $this"
    }
}

fun String.trimmingBearerAuthTokenPrefix(): String {
    var trimmed = this
    // while it starts with the prefix:
    while (trimmed.hasBearerAuthTokenPrefix()) {
        // trim off a prefix!
        trimmed = trimmed.substring(httpHeaderBearerTokenPrefix.length)
    }
    return trimmed
}

fun String.toSpanned(): Spanned {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        return Html.fromHtml(this)
    }
}

@Throws(WriterException::class)
fun String.textToImageEncode(): Bitmap? {
    val bitMatrix: BitMatrix
    try {
        bitMatrix = MultiFormatWriter().encode(this, BarcodeFormat.QR_CODE, 500, 500, null)
    } catch (exception: IllegalArgumentException) {
        return null
    }

    val bitMatrixWidth = bitMatrix.width
    val bitMatrixHeight = bitMatrix.height
    val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)

    for (y in 0 until bitMatrixHeight) {
        val offset = y * bitMatrixWidth
        for (x in 0 until bitMatrixWidth) {
            pixels[offset + x] = if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE
        }
    }
    val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)
    bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight)
    return bitmap
}

@SuppressLint("SimpleDateFormat")
fun String.textToDate():String{
    val parser =  SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val formatter = SimpleDateFormat("yyyy-MM-dd")
    return formatter.format(parser.parse(this))
}

