package hi.studio.msiha.ui.main.acceptance.detailacceptance

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import hi.studio.msiha.R
import hi.studio.msiha.ui.main.acceptance.AcceptanceViewModel
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.bottomsheet_confirm_acceptance.*
import kotlinx.android.synthetic.main.bottomsheet_confirm_acceptance.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ConfirmAcceptanceBottomSheet(val id: String?): BottomSheetDialogFragment() {
    lateinit var status: String
    private val viewModel: AcceptanceViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.bottomsheet_confirm_acceptance, container, false)
        dialog!!.setCanceledOnTouchOutside(false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleSpinner(view)
        observeLiveData()
        onClickButton()
    }

    private fun onClickButton(){

        bt_cancel.setOnClickListener {
            dismiss()
        }
        buttonBsDone.setOnClickListener {
            dismiss()
        }
        bt_save.setOnClickListener {
            when(spinnerAcceptance.selectedItem.toString()){
                "Diterima Penuh" -> status = "ACCEPTED_FULLY"
                "Diterima Sebagian" -> status = "ACCEPTED_DIFFERENT"
                "Paket Hilang" -> status = "LOST"
            }

            if (status == "ACCEPTED_DIFFERENT"){
                val bundle = Bundle()
                bundle.putString("id", id)
                findNavController().navigate(R.id.editAcceptDifferentFragment, bundle)
                dismiss()
            }else{
                viewModel.setStatusAcceptance(id,status)
            }
        }
    }

    private fun handleSpinner(view: View){
        ArrayAdapter.createFromResource(context!!,R.array.statusAcceptance, android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            view.spinnerAcceptance.adapter = adapter
        }
    }

    private fun observeLiveData(){
        viewModel.setStatusAcceptanceResult.observe(viewLifecycleOwner, Observer<Resource<Void>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    wrapperConfirmAcc.visibility = View.GONE
                    wrapBerhasil.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> {

                }
            }
        })
    }
}