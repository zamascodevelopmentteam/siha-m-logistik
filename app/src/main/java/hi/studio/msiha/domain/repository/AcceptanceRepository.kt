package hi.studio.msiha.domain.repository

import hi.studio.msiha.data.entity.request.AcceptanceDifferentRequest
import hi.studio.msiha.data.entity.request.SearchByQrCodeRequest
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.msiha.data.entity.response.OrdersInResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response

interface AcceptanceRepository {
    fun getDetailAcceptance(id:String)
    fun getAllOrderLogisticInAsync():Deferred<Response<OrdersInResponse>>
    fun getOrderDetailLogisticInAsync(id:String):Deferred<Response<AcceptanceDetailResponse>>
    fun getQrCodeByIdAsync(id: SearchByQrCodeRequest):Deferred<Response<AcceptanceDetailResponse>>
    fun setStatusAcceptance(id:String?, status: String):Deferred<Response<Void>>
    fun setStatusAcceptanceDiff(id:String?, data: AcceptanceDifferentRequest):Deferred<Response<Void>>
}