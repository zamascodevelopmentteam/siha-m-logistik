package hi.studio.msiha.ui.main.dashboard

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.AvailableMedicine
import hi.studio.hipechat.util.extension.textToDate
import kotlinx.android.synthetic.main.list_item_request_medicine.view.*


interface RequestMedicineAction {
    fun onEditMedicine(med: AvailableMedicine)
}

class RequestedMedicineAdapter(
    private var availMedicine: List<AvailableMedicine>,
    private val action: RequestMedicineAction
) : RecyclerView.Adapter<RequestedMedicineAdapter.MainViewHolder>() {

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val medName = view.textMed
        val medQty = view.textMedQuantity
        val medExpiredDate = view.textMedExpiredDate
        val buttonEdit = view.buttonEditMed
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
            return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_request_medicine, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return availMedicine.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val current = availMedicine[position]
        Log.d("tag24", current.toString())
        holder.medName.text = current.medicineName
        holder.medQty.text =
            "Jumlah Barang: " + current.requestedQuantity.toString() + " " + current.packageUnit
        holder.medExpiredDate.text = "Expired Date : ${current.expiredDate?.textToDate()}"
        holder.buttonEdit.setOnClickListener {
            action.onEditMedicine(current)
        }
    }

    fun updateMedicine(medicines: ArrayList<AvailableMedicine>) {
        availMedicine = medicines
        notifyDataSetChanged()
    }

    fun getRequestedMedicine(): ArrayList<AvailableMedicine> {
        val arrayListMedicine = ArrayList<AvailableMedicine>()
        arrayListMedicine.addAll(availMedicine)
        return arrayListMedicine
    }
}