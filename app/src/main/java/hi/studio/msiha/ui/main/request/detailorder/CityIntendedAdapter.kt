package hi.studio.msiha.ui.main.dashboard

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.City
import kotlinx.android.synthetic.main.list_city.view.*

interface SelectCity{
    fun onSelectCity(city:City)
}
class CityIntendedAdapter(
    private var cities: List<City>,
    private val selectCity: SelectCity
) : RecyclerView.Adapter<CityIntendedAdapter.MainViewHolder>(),Filterable {

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val cityName = view.city_name
        val wrapper = view.listWrapper
    }

    private var filteredCities = cities

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {

        return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_city, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return filteredCities.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val current = filteredCities[position]
        holder.cityName.text = current.name
        holder.wrapper.setOnClickListener {
            selectCity.onSelectCity(current)
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            @SuppressLint("LogNotTimber")
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val charSearch: String = p0.toString()
                if (charSearch.isEmpty()) {
                    filteredCities = cities
                } else {
                    val resultList = ArrayList<City>()
                    for (row in cities) {
                        if(row.name!!.toLowerCase().contains(charSearch.toLowerCase())) resultList.add(row)
                    }
                    filteredCities = resultList
                }
                val filterResult = FilterResults()
                filterResult.values = filteredCities
                return filterResult
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                filteredCities = p1?.values as ArrayList<City>
                notifyDataSetChanged()
            }
        }
    }


}