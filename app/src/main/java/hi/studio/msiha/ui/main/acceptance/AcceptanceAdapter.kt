package hi.studio.msiha.ui.main.acceptance

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.OrderLogisticIn
import hi.studio.msiha.data.source.local.contentprovider.*
import kotlinx.android.synthetic.main.list_acceptence.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

interface OnOrderClick {
    fun onClick(orderIn: OrderLogisticIn)
}

class AcceptanceAdapter(
    private var acceptanceActive: List<OrderLogisticIn>,
    private var onOrderClick: OnOrderClick
) : RecyclerView.Adapter<AcceptanceAdapter.MainViewHolder>(), Filterable {

    private var filterOrderIn = acceptanceActive

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_acceptence, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return filterOrderIn.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val currentAcceptance = filterOrderIn[position]
        holder.invNum.text = currentAcceptance.droppingNumber
        holder.sender.text = "Pengirim: ${currentAcceptance.senderName}"

        var approvalStatus = currentAcceptance.approvalStatus
        when (approvalStatus) {
            IN_EVALUATION -> approvalStatus = "Review"
            CONFIRMED -> approvalStatus = "Sudah Disetujui"
            APPROVED_FULLY -> approvalStatus = "Buat Usulan Kebutuhan"
            APPROVED_FULL -> approvalStatus = "Buat Usulan Kebutuhan"
            REJECTED_INTERNAL -> approvalStatus = "Tidak Disetujui"
            REJECTED_UPPER -> approvalStatus = "Tidak Membuat Usulan Kebutuhan"
            DRAFT -> approvalStatus = "Draft"
        }


        holder.statusAcc.text = "Status: $approvalStatus"

        if (!currentAcceptance.updatedAt.isNullOrEmpty()) {
            val format2 = "yyyy-MM-dd"
            val localeID = Locale("in", "ID")
            val simpleDateFormat2 = SimpleDateFormat(format2, localeID)
            val date = simpleDateFormat2.parse(currentAcceptance.updatedAt)
            holder.lastUpdate.text = "Terakhir Update: ${simpleDateFormat2.format(date)}"
        }

        holder.wrapper.setOnClickListener {
            onOrderClick.onClick(currentAcceptance)
        }
    }

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val invNum = view.tv_no_inv_dist
        val sender = view.tv_sender_dist
        val lastUpdate = view.tv_last_update_dist
        val wrapper = view.wrapperAcceptance
        val statusAcc = view.tv_status_dist
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            @SuppressLint("LogNotTimber")
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val charSearch: String = p0.toString()
                if (charSearch.isEmpty()) {
                    filterOrderIn = acceptanceActive
                } else {
                    val resultList = ArrayList<OrderLogisticIn>()
                    for (row in acceptanceActive) {
                        if (row.droppingNumber!!.toLowerCase().contains(charSearch.toLowerCase())) resultList.add(
                            row
                        )
                    }
                    filterOrderIn = resultList
                }
                val filterResult = FilterResults()
                filterResult.values = filterOrderIn
                return filterResult
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                filterOrderIn = p1?.values as ArrayList<OrderLogisticIn>
                notifyDataSetChanged()
            }
        }
    }
}