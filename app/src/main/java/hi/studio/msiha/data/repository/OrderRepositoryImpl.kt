package hi.studio.msiha.data.repository

import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.entity.request.AddOrderRequest
import hi.studio.msiha.data.entity.response.AvailableMedicineResponse
import hi.studio.msiha.data.entity.response.CityResponse
import hi.studio.msiha.data.entity.response.OrderActiveResponse
import hi.studio.msiha.data.entity.response.OrderDetailResponse
import hi.studio.msiha.data.source.local.contentprovider.TOKEN
import hi.studio.msiha.data.source.network.Endpoint
import hi.studio.msiha.domain.repository.OrderRepository
import kotlinx.coroutines.Deferred
import retrofit2.Response

class OrderRepositoryImpl(private val endpoint: Endpoint) : OrderRepository {

    private var token: String = Hawk.get(TOKEN)

    override fun getAllActiveOrder(): Deferred<Response<OrderActiveResponse>> {
        return endpoint.getAllActiveOrderAsync("Bearer " + Hawk.get(TOKEN))
    }

    override fun getAllActiveOrderOut(): Deferred<Response<OrderActiveResponse>> {
        return endpoint.getRequestAsync("Bearer " + Hawk.get(TOKEN))
    }

    override fun getAvailMed(): Deferred<Response<AvailableMedicineResponse>> {
        return endpoint.getAvailMedAsync("Bearer " + Hawk.get(TOKEN))
    }

    override fun getAllCities(): Deferred<Response<CityResponse>> {
        return endpoint.getAllCityAsync("Bearer " + Hawk.get(TOKEN))
    }

    override fun sendOrderRequest(addOrderRequest: AddOrderRequest): Deferred<Response<Void>> {
        return endpoint.sendOrderRequestAsync("Bearer " + Hawk.get(TOKEN), addOrderRequest)
    }

    override fun getDetailRequest(id: String): Deferred<Response<OrderDetailResponse>> {
        return endpoint.getDetailRequest(id, "Bearer " + Hawk.get(TOKEN))
    }
}