package hi.studio.msiha.data.entity.response

class LoginResponse(
    val data: Data
)

data class Data(
    val user: User,
    val token: String
)