package hi.studio.msiha.ui.auth.signin

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.orhanobut.hawk.Hawk

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.response.LoginResponse
import hi.studio.msiha.data.source.local.contentprovider.TOKEN
import hi.studio.msiha.ui.main.MainActivity
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class SignInFragment : Fragment() {

    private val viewModel: SignInViewModel by viewModel()
    private lateinit var token: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(Hawk.contains(TOKEN)){
            startActivity(Intent(activity,MainActivity::class.java))
            activity?.finish()
        }else{
            handleSignIn(view)
        }

    }

    override fun onResume() {
        super.onResume()
        observeLiveData()
    }

    private fun handleSignIn(view: View){
        view.buttonLogin.setOnClickListener {
            viewModel.login(view.editTextNik.text.toString(), view.editTextPassword.text.toString())
            signInProgressBar.visibility=View.VISIBLE
            textButtonLogin.text = ""
        }
    }

    private fun observeLiveData(){
        viewModel.loginResult.observe(this, Observer<Resource<LoginResponse>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    signInProgressBar.visibility=View.INVISIBLE
                    textButtonLogin.text = "Masuk"
                    startActivity(Intent(activity,MainActivity::class.java))
                    activity?.finish()
                }
                Status.ERROR -> {
                    Toast.makeText(context,"error ${it.message}", Toast.LENGTH_SHORT).show()
                    textButtonLogin.text = "Masuk"
                    signInProgressBar.visibility=View.INVISIBLE
                }
                Status.LOADING -> {
                    textButtonLogin.text = ""
                    signInProgressBar.visibility=View.VISIBLE
                }
            }
        })
    }

}
