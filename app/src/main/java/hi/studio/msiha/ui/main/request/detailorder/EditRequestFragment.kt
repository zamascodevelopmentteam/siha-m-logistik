package hi.studio.msiha.ui.main.request.detailorder


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.City
import kotlinx.android.synthetic.main.fragment_edit_order.view.*

/**
 * A simple [Fragment] subclass.
 */
class EditRequestFragment : Fragment(),GetSelectedCity {

    override fun getSelectedCity(city: City) {

    }

    private lateinit var cityIntendedBottomSheet: CityIntendedBottomSheet

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleSpinner(view)
        handleBottomSheet(view)
    }

    private fun handleSpinner(view: View){
        ArrayAdapter.createFromResource(context!!,R.array.statusSpinner, android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            view.spinnerStatusOrder.adapter = adapter
            view.spinnerStatusAppOrder.adapter = adapter
        }
    }

    private fun handleBottomSheet(view: View){
        cityIntendedBottomSheet = CityIntendedBottomSheet(this)
        view.cityIntended.setOnClickListener {
            cityIntendedBottomSheet.show(activity!!.supportFragmentManager, cityIntendedBottomSheet.tag)
        }

    }



}
