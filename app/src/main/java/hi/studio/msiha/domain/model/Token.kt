package hi.studio.msiha.domain.model

import com.google.gson.Gson

class Token(var access:String,var refresh:String,var ttl:String){
    companion object {
        @JvmStatic
        fun toJSON(gson: Gson): String {
            return gson.toJson(this)
        }

        @JvmStatic
        fun fromJSON(gson: Gson, data: String): Token {
            return gson.fromJson(data, Token::class.java)
        }

        fun empty() = Token("", "", "")
    }
}