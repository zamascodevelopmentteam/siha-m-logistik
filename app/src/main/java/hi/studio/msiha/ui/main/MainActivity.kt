package hi.studio.msiha.ui.main

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomnavigation.BottomNavigationMenu
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.AvailableMedicine
import hi.studio.msiha.data.entity.response.User
import hi.studio.msiha.data.source.local.contentprovider.DEVICE_TOKEN
import hi.studio.msiha.data.source.local.contentprovider.USER
import kotlinx.android.synthetic.main.activity_main.*

interface RequestMedicine {
    fun addMedicine(medicine: AvailableMedicine)
    fun getRequestedMedicine(): ArrayList<AvailableMedicine>
    fun clearRequestedMedicineList()
}

class MainActivity : AppCompatActivity(), RequestMedicine {

    private lateinit var mNavController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var mBottomNav: BottomNavigationView
    private lateinit var host: NavHostFragment
    private lateinit var mToolbar: androidx.appcompat.widget.Toolbar

    private var requestedMedicine = ArrayList<AvailableMedicine>()
    private lateinit var bottomNavigationMenu: BottomNavigationMenu


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setThemeByRole()
        setContentView(R.layout.activity_main)
        setupNavController()
        setupBottomNavMenu(mNavController)
        setupActionBar(mNavController, appBarConfiguration)
    }

    private fun handleNotification() {
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("tag23w", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                Hawk.put(DEVICE_TOKEN, token)
            })


    }

    private fun setThemeByRole() {
        when ((Hawk.get(USER) as User).logisticrole.toString()) {
            "UPK_ENTITY" -> setTheme(R.style.AppThemeUpk)
            "SUDIN_ENTITY" -> setTheme(R.style.AppThemeSudin)
            "PROVINCE_ENTITY" -> setTheme(R.style.AppThemeProvinsi)
            "MINISTRY_ENTITY" -> setTheme(R.style.AppThemeKemenkes)
        }

    }


    override fun addMedicine(medicine: AvailableMedicine) {
        val idx = requestedMedicine.indexOfFirst {
            it.medicineName == medicine.medicineName
        }
        if (idx == -1) {
            requestedMedicine.add(medicine)
        } else {
            if (medicine.requestedQuantity == 0) {
                requestedMedicine.removeAt(idx)
            } else {
                requestedMedicine[idx] = medicine
            }
        }
    }

    override fun getRequestedMedicine(): ArrayList<AvailableMedicine> {
        return requestedMedicine
    }

    override fun clearRequestedMedicineList() {
        requestedMedicine.clear()
    }

    private fun setupNavController() {

        host = supportFragmentManager
            .findFragmentById(R.id.homeNavHost) as NavHostFragment? ?: return
        mNavController = host.navController
        when ((Hawk.get(USER) as User).logisticrole.toString()) {
            "UPK_ENTITY" -> setTheme(R.style.AppThemeUpk)
            "SUDIN_ENTITY" -> setTheme(R.style.AppThemeSudin)
            "PROVINCE_ENTITY" -> setTheme(R.style.AppThemeProvinsi)
            "MINISTRY_ENTITY" -> setTheme(R.style.AppThemeKemenkes)
        }
            appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.dashboardFragment,
                    R.id.requestMainFragment,
                    R.id.distributionFragment,
                    R.id.acceptanceFragment,
                    R.id.profileFragment
                )
            )
    }

    private fun setupActionBar(
        navController: NavController,
        appBarConfig: AppBarConfiguration
    ) {
        mToolbar = findViewById(R.id.defaultToolbar)
        setSupportActionBar(mToolbar)
        setupActionBarWithNavController(navController, appBarConfig)
    }

    private fun hideBottomNavigation() {
        // bottom_navigation is BottomNavigationView
        with(mBottomNav) {
            if (visibility == View.VISIBLE && alpha == 1f) {
                animate()
                    .alpha(0f)
                    .withEndAction { visibility = View.GONE }
                    .duration = 0
            }
        }
    }

    private fun hideRequestMenu(){
        with(mBottomNav){
            mBottomNav.menu.findItem(R.id.requestMainFragment).setVisible(false)
        }
    }

    private fun showBottomNavigation() {
        // bottom_navigation is BottomNavigationView
        with(mBottomNav) {
            visibility = View.VISIBLE
            animate()
                .alpha(1f)
                .duration = 500
        }
    }

    private fun setupBottomNavMenu(navController: NavController) {
        mBottomNav = findViewById(R.id.homeBottomNav)
        mBottomNav.setupWithNavController(navController)

        if((Hawk.get(USER) as User).logisticrole.toString()=="LAB_ENTITY" ||
            (Hawk.get(USER) as User).logisticrole.toString()=="PHARMA_ENTITY"  ){
            mNavController.addOnDestinationChangedListener { _, destination, _ ->
                if (destination.id == R.id.dashboardFragment ||
                    destination.id == R.id.requestMainFragment ||
                    destination.id == R.id.distributionFragment ||
                    destination.id == R.id.acceptanceFragment ||
                    destination.id == R.id.profileFragment

                ) {
                    showBottomNavigation()
                    hideRequestMenu()
                } else {
                    hideBottomNavigation()
                }
            }

        }else{
            mNavController.addOnDestinationChangedListener { _, destination, _ ->
                if (destination.id == R.id.dashboardFragment ||
                    destination.id == R.id.requestMainFragment ||
                    destination.id == R.id.distributionFragment ||
                    destination.id == R.id.acceptanceFragment ||
                    destination.id == R.id.profileFragment
                ) {
                    showBottomNavigation()
                } else {
                    hideBottomNavigation()
                }
            }
        }


    }

    public fun hideSoftKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(this.currentFocus?.windowToken, 0)
    }

    override fun onSupportNavigateUp(): Boolean {
        hideSoftKeyboard()
        return findNavController(R.id.homeNavHost).navigateUp(appBarConfiguration)
    }
}
