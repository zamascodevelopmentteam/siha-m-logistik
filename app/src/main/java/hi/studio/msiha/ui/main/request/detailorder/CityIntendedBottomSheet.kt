package hi.studio.msiha.ui.main.request.detailorder

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.City
import hi.studio.msiha.data.entity.response.CityResponse
import hi.studio.msiha.ui.main.dashboard.CityIntendedAdapter
import hi.studio.msiha.ui.main.dashboard.DashboardViewModel
import hi.studio.msiha.ui.main.dashboard.SelectCity
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.bottomsheet_city_intended.*
import kotlinx.android.synthetic.main.bottomsheet_city_intended.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel

interface GetSelectedCity{
    fun getSelectedCity(city: City)
}
class CityIntendedBottomSheet(private val getSelectedCity: GetSelectedCity) : BottomSheetDialogFragment(),SelectCity {

    private lateinit var cityIntendedAdapter: CityIntendedAdapter
    private val viewModel: DashboardViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.bottomsheet_city_intended, container, false)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.setOnShowListener { dialogInterface ->
            val bottomSheetDialog = dialogInterface as BottomSheetDialog
            setupFullHeight(bottomSheetDialog)
        }
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.imgCloseBs.setOnClickListener {
            dismiss()
        }
        observeLiveData()
        setupFilter()
        viewModel.getCities()
    }

    override fun onSelectCity(city: City) {
        getSelectedCity.getSelectedCity(city)
        this.dismiss()
    }

    private fun setupFullHeight(bottomSheetDialog: BottomSheetDialog) {
        val bottomSheet =
            bottomSheetDialog.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?
        val behavior = BottomSheetBehavior.from(bottomSheet!!)
        val layoutParams = bottomSheet.layoutParams

        val windowHeight = getWindowHeight()
        if (layoutParams != null) {
            layoutParams.height = windowHeight
        }
        bottomSheet.layoutParams = layoutParams
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun getWindowHeight(): Int {
        // Calculate window height for fullscreen use
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

    private fun setupFilter(){
        searchCity.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
               // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
               // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                cityIntendedAdapter.filter.filter(p0.toString())
            }

        })
    }
    private fun handleCity(city: ArrayList<City>){
        cityIntendedAdapter = CityIntendedAdapter(city, this)

        rvListCity.apply {
            adapter = cityIntendedAdapter
            layoutManager = LinearLayoutManager(activity) as RecyclerView.LayoutManager?
        }

    }

    private fun observeLiveData(){
        viewModel.allCities.observe(this, Observer<Resource<CityResponse>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        handleCity(it.data.data)
                        Log.d("tag4", it.data.data.toString())
                    } else {
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> {
                }
            }
        })
    }



}