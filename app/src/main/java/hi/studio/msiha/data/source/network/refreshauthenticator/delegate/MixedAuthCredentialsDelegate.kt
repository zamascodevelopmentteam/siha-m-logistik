/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.hipechat.data.source.network.refreshauthenticator.delegate


/**
 * Created by mochadwi on 2019-11-03
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

interface MixedAuthCredentialsDelegate : OauthRefreshAuthenticatorDelegate {
    // Used to retrieve the oauth client's basic client id/secret auth header
    fun clientBasicAuthCredentials(): String

    // Used to retrieve the user's basic user/pass credentials auth header
    fun userBasicAuthCredentials(): String
}