package hi.studio.msiha.data.repository

import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.msiha.data.entity.response.DistributionResponse
import hi.studio.msiha.data.source.local.contentprovider.TOKEN
import hi.studio.msiha.data.source.network.Endpoint
import hi.studio.msiha.domain.repository.DistributionRepository
import kotlinx.coroutines.Deferred
import retrofit2.Response

class DistributionRepositoryImpl(private val endpoint: Endpoint) : DistributionRepository {

    private var token: String = Hawk.get(TOKEN)

    override fun getAllDistribution(): Deferred<Response<DistributionResponse>> {
        return endpoint.getAllDistributionAsycn("Bearer " + Hawk.get(TOKEN))
    }

    override fun getDetailRequest(id: String): Deferred<Response<AcceptanceDetailResponse>> {
        return endpoint.getDetailLogisticInAsync("Bearer " + Hawk.get(TOKEN), id)
    }
}