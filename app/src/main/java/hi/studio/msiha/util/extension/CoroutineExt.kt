/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.hipechat.util.extension

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


/**
 * Created by mochadwi on 2019-10-30
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

// Ref: https://medium.com/@sampsonjoliver/achieving-async-await-in-the-android-wasteland-a6fe30dbaaa1
fun coroutineLaunch(
    context: CoroutineContext = Dispatchers.Default,
    runner: suspend CoroutineScope.() -> Unit
): Job =
    CoroutineScope(context).launch { runner.invoke((this)) }
