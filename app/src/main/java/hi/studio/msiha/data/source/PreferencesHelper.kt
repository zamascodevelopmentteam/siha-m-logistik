package hi.studio.msiha.data.source

import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.net.Uri
import hi.studio.msiha.BuildConfig
import timber.log.Timber

const val PREF_USER_PASSWORD = "PREF_USER_PASSWORD"
const val PREF_USER_CHOSEN_WORKSPACE = "PREF_USER_CHOSEN_WORKSPACE"

object PreferencesHelper {
    val AUTHORITY_NAME = "${BuildConfig.APPLICATION_ID}.provider.sharedpref"

    //TODO add all prefHelper constant that need to cleanef

    val CONTENT_URI = Uri.parse("content://$AUTHORITY_NAME/sharedpref")

    fun getStringPrefUri(pref: String) = Uri.parse("$CONTENT_URI/1/$pref")

    fun getStringPreference(contentResolver: ContentResolver, pref: String): String {
        var value=""
        val cursor = contentResolver.query(getStringPrefUri(pref), null, null, null, null)
        cursor.use { c ->
            if (c == null) {
                return value
            }

            if (c.moveToFirst()) {
                do {
                    value = c.getString(c.getColumnIndex(pref))
                } while (c.moveToNext())
            }
        }
        cursor?.close()

        return value
    }

    fun setStringPreference(contentResolver: ContentResolver, pref: String, value: String) {
        val contentValues = ContentValues()
        contentValues.put(pref, value)
        contentResolver.insert(getStringPrefUri(pref), contentValues)
    }

    fun setIntPreference(contentResolver: ContentResolver, pref: String, value: Int) {
        val contentValues = ContentValues()
        contentValues.put(pref, value)
        contentResolver.insert(getStringPrefUri(pref), contentValues)
    }

    fun getIntPreference(contentResolver: ContentResolver, pref: String, defaultValue: Int): Int {
        val c = contentResolver.query(getStringPrefUri(pref), null, null, null, null)
        var value = defaultValue
        if (c == null) {
            return value
        }

        if (c.moveToFirst()) {
            do {
                value = try {
                    c.getInt(c.getColumnIndex(pref))
                }catch (exception:NumberFormatException){
                    0
                }
            } while (c.moveToNext())
        }

        return value
    }

    fun setBooleanPreference(contentResolver: ContentResolver, pref: String, value: Boolean) {
        val contentValues = ContentValues()
        contentValues.put(pref, value)
        contentResolver.insert(getStringPrefUri(pref), contentValues)
    }

    fun getBooleanPreference(contentResolver: ContentResolver, pref: String): Boolean {
        val c = contentResolver.query(getStringPrefUri(pref), null, null, null, null)
        var value = false
        if (c == null) {
            return value
        }

        if (c.moveToFirst()) {
            do {
                value = c.getString(c.getColumnIndex(pref))!!.toBoolean()
            } while (c.moveToNext())
        }

        return value
    }

    fun getBooleanPreference(contentResolver: ContentResolver, pref: String, default: Boolean): Boolean {
        Timber.d("get boolean preference")
        val c = contentResolver.query(getStringPrefUri(pref), null, null, null, null)
        var value = default
        c.use { c ->
            if (c == null) {
                Timber.d("get boolean preference null")
                return value
            }

            if (c.moveToFirst()) {
                Timber.d("get boolean preference not null")
                do {
                    val prefValue = c.getString(c.getColumnIndex(pref))!!
                    value = if (prefValue.isEmpty()) default else prefValue.toBoolean()
                } while (c.moveToNext())
            }
        }

        return value
    }

    // Clear all PrefHelper Value
    fun clearAll(context: Context) {


    }

}