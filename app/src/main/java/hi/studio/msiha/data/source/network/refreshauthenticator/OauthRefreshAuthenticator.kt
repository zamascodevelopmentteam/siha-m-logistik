/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA 
 */

package hi.studio.hipechat.data.source.network.refreshauthenticator

import hi.studio.hipechat.data.source.network.refreshauthenticator.delegate.OauthRefreshAuthenticatorDelegate
import hi.studio.hipechat.util.extension.logD
import hi.studio.hipechat.util.extension.withBearerAuthTokenPrefix
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route


/**
 * Created by mochadwi on 2019-11-03
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

const val httpHeaderAuthorization = "Authorization"
const val httpHeaderBearerTokenPrefix = "Bearer"
const val httpHeaderRetryCount = "Retry-Count"
const val OAUTH_RE_AUTH_RETRY_LIMIT = 100

class OauthRefreshAuthenticator(
    val delegate: OauthRefreshAuthenticatorDelegate
) : Authenticator {

    override fun authenticate(route: Route?, response: Response?): Request? {
        if (response?.code() == 500) logD("authenticate with 500")
        if (response?.code() == 404) logD("authenticate with 404")

        logD("Detected authentication error ${response?.code()} on ${response?.request()?.url()}")
        // Did our request have a bearer authToken in it?
        when (hasBearerAuthorizationToken(response)) {
            false -> {
                // No bearer auth authToken; nothing to refresh!
                logD("No bearer authentication to refresh.")
                return null
            }
            true -> {
                // It had a bearer auth!
                logD("Bearer authentication present!")
                val previousRetryCount = retryCount(response)
                // Attempt to reauthenticate using the refresh authToken!
                return reAuthenticateRequestUsingRefreshToken(
                    response?.request(),
                    previousRetryCount + 1
                )
            }
        }
    }

    private fun retryCount(response: Response?): Int {
        return response?.request()?.header(httpHeaderRetryCount)?.toInt() ?: 0
    }

    private fun hasBearerAuthorizationToken(response: Response?): Boolean {
        response?.let { response ->
            val authorizationHeader = response.request().header(httpHeaderAuthorization)
            return authorizationHeader?.startsWith(httpHeaderBearerTokenPrefix) ?: false
        }
        return false
    }

    @Synchronized
    // We synchronize this request, so that multiple concurrent failures
    // don't all try to use the same refresh authToken!
    private fun reAuthenticateRequestUsingRefreshToken(
        staleRequest: Request?,
        retryCount: Int
    ): Request? {

        // See if we have gone too far:
        if (retryCount > OAUTH_RE_AUTH_RETRY_LIMIT) {
            // Yup!
            logD("Retry count exceeded! Giving up.")
            // Don't try to re-authenticate any more.
            return null
        }

        // We have some retries left!
        logD("Attempting to fetch a new authToken...")

        // Try for the new authToken:
        delegate.userRequestNewBearerToken()?.let { newBearerToken ->
            logD("Retreived new authToken, re-authenticating with: $newBearerToken")
            return rewriteRequest(staleRequest, retryCount, newBearerToken)
        }

        // Could not retrieve new authToken! Unable to re-authenticate!
        logD("Failed to retrieve new authToken, unable to re-authenticate!")
        return null
    }


    private fun rewriteRequest(
        staleRequest: Request?, retryCount: Int, authToken: String
    ): Request? {
        return staleRequest?.newBuilder()
            ?.header(
                httpHeaderAuthorization,
                authToken.withBearerAuthTokenPrefix()
            )
            ?.header(
                httpHeaderRetryCount,
                "$retryCount"
            )
            ?.build()
    }

}
