package hi.studio.msiha.data

class AvailableOrderFor(
    val name: String?,
    val upkId: String?,
    val sudinkotakabid: String?,
    val provinceId: String?,
    val centralministry: String
)