package hi.studio.msiha.ui.main.request.detailorder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.response.OrderItems
import kotlinx.android.synthetic.main.detail_acceptance_med_item.view.*
import kotlinx.android.synthetic.main.detail_acceptance_med_item.view.textBatchCode
import java.text.SimpleDateFormat
import java.util.*

class DetailRequestAdapter (val items: List<OrderItems>) :
    RecyclerView.Adapter<DetailRequestAdapter.MainViewHolder>() {

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var textMedName = view.textMedName
        var textJenisBarang = view.textJenisBarang
        var textJumlahBarang = view.textJumlahBarang
        var textTanggalMasuk = view.textTanggalMasuk
        var textBatchCode = view.textBatchCode
        var textExpiredDate = view.textExpiredDate
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.detail_acceptance_med_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val current = items[position]
        val format2 = "yyyy-MM-dd"
        val localeID = Locale("in", "ID")
        val simpleDateFormat2 = SimpleDateFormat(format2, localeID)
        if(current.expiredDate!=null){
            val date = simpleDateFormat2.parse(current.expiredDate)
            val expired = simpleDateFormat2.format(date)
            holder.textExpiredDate.text = "Expired Data: $expired"
        }
        holder.textTanggalMasuk.visibility = View.GONE
        holder.textJenisBarang.visibility = View.GONE

        holder.textMedName.text = current.medicine.codeName
        holder.textJumlahBarang.text = current.packageQuantity+ " " + current.packageUnitType
//        holder.textBatchCode.text = current.createdAt

    }
}