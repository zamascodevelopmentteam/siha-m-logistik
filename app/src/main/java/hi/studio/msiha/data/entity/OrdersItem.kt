package hi.studio.msiha.data.entity

data class OrdersItem(
    val ordersItem: String,
    var medicineId: Int? = 0,
    val medincineName: Int? = 0,
    val packageUnit: String? = null,
    val packageQuantity: Int? = 0,
    val expiredDate: String? = null
) {
}