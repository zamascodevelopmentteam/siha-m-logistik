package hi.studio.msiha.data.entity

import com.google.gson.annotations.SerializedName

data class DetailAcceptance(
    val id: String,
    var droppingNumber: String? = null,
    val picId: String? = null,
    val fund_source: String? = null,
    var planStatus: String? = null,
    var approvalStatus: String? = null,
    var logisticRole: String? = null,
    var createdBy: String? = null,
    var notes: String? = null,
    var approvalNotes: String? = null,
    var updatedBy: Int? = 0,
    var provinceId: String? = null,
    var sudinKabKotaId: String? = null,
    var upkId: Int? = 0,
    var createdAt: String? = null,
    var updatedAt: String? = null,
    var deletedAt: String? = null,
    var orderId: String? = null,
    @SerializedName("distribution_plans_items")
    val distributionPlanItems: List<DistributionPlanItems>,
    var senderName: String? = null,
    var recieverName: String? = null
    )