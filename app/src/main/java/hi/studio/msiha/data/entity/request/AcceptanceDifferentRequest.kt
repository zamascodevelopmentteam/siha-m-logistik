package hi.studio.msiha.data.entity.request

class AcceptanceDifferentRequest(
    val distributionPlansItems: ArrayList<DistributionPlans>
)

class DistributionPlans(
    private val id: Int?,
    private val actualReceivedPackageQuantity: Int?
)