package hi.studio.msiha.ui.main.acceptance

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import hi.studio.msiha.R
import kotlinx.android.synthetic.main.bottomsheet_confirm_acceptance.*

class BottomSheetSuccess : BottomSheetDialogFragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.bottomsheet_confirm_acceptance, container, false)
        dialog!!.setCanceledOnTouchOutside(false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonBsDone.setOnClickListener {
            dismiss()
        }
        wrapperConfirmAcc.visibility = View.GONE
        wrapBerhasil.visibility = View.VISIBLE

    }
}