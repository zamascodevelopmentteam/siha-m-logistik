package hi.studio.msiha.data.entity

data class City(
    var name: String? = "",
    var upkid: Int?=0,
    var sudinkotakabid: Int?=0,
    var provinceid: Int = 0,
    var centralministry: String? = ""
)