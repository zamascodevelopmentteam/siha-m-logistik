package hi.studio.msiha.data.entity.response

class Medicine (
    val medicineId:Int,
    val medicineName:String,
    val packageUnitType:String,
    val expiredDate:String,
    val batchCode:String
)