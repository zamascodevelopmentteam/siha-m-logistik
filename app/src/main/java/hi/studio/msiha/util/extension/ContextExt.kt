/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.hipechat.util.extension

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment


/**
 * Created by mochadwi on 2019-10-23
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

lateinit var toast: Toast

// TODO(mochadwi): 2019-10-31 Modify all the hardcoded string with string.xml
fun Context.showToast(msg: String, length: Int = Toast.LENGTH_SHORT) {
    if (::toast.isInitialized) toast.cancel()
    toast = Toast.makeText(this, msg, length)
    toast.show()
}

fun Context.showToast(@StringRes msg: Int, length: Int = Toast.LENGTH_SHORT) {
    showToast(getString(msg), length)
}

fun Fragment.showToast(msg: String) {
    requireContext().showToast(msg)
}