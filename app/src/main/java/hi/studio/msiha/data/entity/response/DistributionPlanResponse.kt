package hi.studio.msiha.data.entity.response

class DistributionPlanResponse(
    val droppingNumber:String,
    val orderId:Int,
    val picId:Int,
    val fundSource:String,
    val planStatus:String,
    val logisticRole:String,
    val provinceId:Int,
    val sudinKabKotaId:Int,
    val upkId:Int,
    val senderName:String,
    val receiverName:String,
    val lastUpdated:String,
    val items:ArrayList<Medicine>
)