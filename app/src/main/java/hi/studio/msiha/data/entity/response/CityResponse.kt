package hi.studio.msiha.data.entity.response

import hi.studio.msiha.data.entity.City

class CityResponse(val data: ArrayList<City>) {
}