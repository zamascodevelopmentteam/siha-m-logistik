package hi.studio.msiha.ui.auth.signin

import androidx.lifecycle.ViewModel
import hi.studio.msiha.data.entity.response.LoginResponse
import hi.studio.msiha.domain.interactor.GetAuth
import hi.studio.hipechat.util.LiveEvent
import hi.studio.hipechat.util.Resource

class SignInViewModel(private val getAuth: GetAuth):ViewModel() {

    val loginResult: LiveEvent<Resource<LoginResponse>>
        get() = getAuth.loginResult
    fun login(email:String,password:String){
        getAuth.login(email,password)
    }
}