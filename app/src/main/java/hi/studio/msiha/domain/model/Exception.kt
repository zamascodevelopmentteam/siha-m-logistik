/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.msiha.domain.model


/**
 * Created by mochadwi on 2019-10-21
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

/**
 * Base Class for handling errors/failures/exceptions.
 * Every feature specific failure should extend [FeatureFailure] class.
 */
sealed class Failure : Throwable() {
    object NetworkConnection : Failure()
    object ServerError : Failure()
    data class MethodNotAllowed(override val message: String?) : Failure()
    data class BadRequest(override val message: String?) : Failure()

    /** * Extend this class for feature specific failures.*/
    abstract class FeatureFailure: Failure()
}