package hi.studio.msiha.data.entity.response

import com.google.gson.annotations.SerializedName

data class User(
    val id: Int?,
    val avatar: String?,
    val createdAt: String?,
    @SerializedName("created_by")
    val createdBy: Int?,
    val email: String?,
    val isActive: Boolean?,
    val logisticrole: Any?,
    val name: String?,
    val nik: String?,
    val phone: String?,
    val provinceId: Int?,
    val role: String?,
    val sudinKabKotumId: Int?,
    val updatedAt: String?,
    @SerializedName("updated_by")
    val updatedBy: Int?,
    val upkId: Int?
)