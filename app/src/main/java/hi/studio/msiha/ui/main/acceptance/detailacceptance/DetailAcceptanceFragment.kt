package hi.studio.msiha.ui.main.acceptance.detailacceptance


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.orhanobut.hawk.Hawk

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.DetailAcceptance
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.msiha.data.entity.response.User
import hi.studio.msiha.data.source.local.contentprovider.*
import hi.studio.msiha.ui.main.MainActivity
import hi.studio.msiha.ui.main.acceptance.AcceptanceViewModel
import hi.studio.msiha.util.view.QrCodeBottomSheet
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.fragment_detail_acceptance.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class DetailAcceptanceFragment : Fragment() {

    private var acceptanceId="default"
    private val viewModel: AcceptanceViewModel by viewModel()
    private lateinit var acceptenceMedAdapter: AcceptanceMedListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if((Hawk.get(USER) as User).logisticrole.toString() == "MINISTRY_ENTITY"){
            (activity as MainActivity).supportActionBar?.title = "Detail Pembelian"
        }else{
            (activity as MainActivity).supportActionBar?.title = "Detail Penerimaan"
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_acceptance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProgressBar()
        val id = arguments?.getString("id").toString()
        viewModel.getOrdersDetailIn(id)
        setupShowQrButton(acceptanceId)
        observeLiveData()
    }

    override fun onResume() {
        super.onResume()
        observeLiveData()
    }

    private fun handleConfirm(id: String?){
        val confirmBottomSheet = ConfirmAcceptanceBottomSheet(id)
        buttonConfirmReceive.setOnClickListener {
            confirmBottomSheet.show(activity!!.supportFragmentManager, confirmBottomSheet.tag)
        }
    }

    private fun setupShowQrButton(id:String?){
        buttonShowQR.setOnClickListener {
            val bottomSheet = QrCodeBottomSheet(id)
            activity?.supportFragmentManager?.let { bottomSheet.show(it, "show_qr") }
        }
    }

    private fun bindItem(detailAcceptance: DetailAcceptance){

        if(!detailAcceptance.updatedAt.isNullOrEmpty()){
            val format2 = "yyyy-MM-dd"
            val localeID = Locale("in", "ID")
            val simpleDateFormat2 = SimpleDateFormat(format2, localeID)
            val date = simpleDateFormat2.parse(detailAcceptance.updatedAt)
            textLastUpdated.text = simpleDateFormat2.format(date)
        }

        var accStatus = detailAcceptance.planStatus
        when (accStatus) {
            EXECUTED -> accStatus = "Diterima"
            DRAFT -> accStatus = "Draft"
            PROCESSED -> accStatus = "Dalam Proses"
            EVALUATION -> accStatus = "Dalam Evaluasi"
            ACCEPTED_FULLY -> accStatus = "Diterima Penuh"
            ACCEPTED_DIFFERENT -> accStatus = "Diterima Sebagian"
            LOST -> accStatus = "Hilang"
            REJECTED -> accStatus = "Ditolah"
        }

        if(detailAcceptance.planStatus == PROCESSED){
            buttonConfirmReceive.visibility = View.VISIBLE
        }

        acceptenceMedAdapter = AcceptanceMedListAdapter(detailAcceptance.distributionPlanItems)

        recyclerViewMed.apply {
            adapter = acceptenceMedAdapter
            layoutManager = LinearLayoutManager(activity)
        }

        textInv.text = detailAcceptance.droppingNumber
        textReceiverStatus.text = accStatus
        textRecipient.text = detailAcceptance.recieverName
        textSender.text = detailAcceptance.senderName

        setupShowQrButton(detailAcceptance.droppingNumber)
        handleConfirm(detailAcceptance.id)
    }

    private fun showProgressBar(){
        progressBarDetailAcceptance.visibility = View.VISIBLE
        wrapDetail.visibility = View.GONE
    }

    private fun hideProgressBar(){
        progressBarDetailAcceptance.visibility = View.GONE
        wrapDetail.visibility = View.VISIBLE
    }

    private fun observeLiveData(){
        viewModel.getOrdersDetailInResult.observe(viewLifecycleOwner, Observer<Resource<AcceptanceDetailResponse>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    if (it.data?.data != null) {
                        bindItem(it.data.data)
                        hideProgressBar()
                    } else {
                        hideProgressBar()
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> {
                }
            }
        })
    }
}
