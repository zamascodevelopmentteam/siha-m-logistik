package hi.studio.msiha.data.repository

import hi.studio.msiha.data.entity.request.UserLoginRequest
import hi.studio.msiha.data.entity.response.LoginResponse
import hi.studio.msiha.data.source.network.Endpoint
import hi.studio.msiha.domain.repository.AuthRepository
import kotlinx.coroutines.Deferred
import retrofit2.Response

class AuthRepositoryImpl(
    private val endpoint: Endpoint
): AuthRepository {
    override fun login(nik: String, password: String): Deferred<Response<LoginResponse>> {
        return endpoint.loginAsync(UserLoginRequest(nik,password))
    }
}