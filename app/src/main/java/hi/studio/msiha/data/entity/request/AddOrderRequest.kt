package hi.studio.msiha.data.entity.request

import hi.studio.msiha.data.entity.AvailableMedicine

class AddOrderRequest(
    private val upkId:Int?,
    private val sudinKabKotaId:Int?,
    private val provinceId:Int?,
    private val orderNotes:String?,
    private val ordersItems:ArrayList<AvailableMedicine>?
)