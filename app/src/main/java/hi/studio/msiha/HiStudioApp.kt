package hi.studio.msiha

import android.app.Application
import com.facebook.stetho.Stetho
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.source.network.InternetConnectionListener
import net.danlew.android.joda.JodaTimeAndroid
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class HiStudioApp : Application()
{
    var mInternetConnectionListener: InternetConnectionListener? = null

    override fun onCreate(){
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        JodaTimeAndroid.init(this)
        Stetho.initializeWithDefaults(this)
        Hawk.init(this@HiStudioApp).build()
        // start Koin!
        startKoin {
            // Android context
            androidContext(this@HiStudioApp)
            // modules
            modules(hiStudioModule)
        }
    }
}