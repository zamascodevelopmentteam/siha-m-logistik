package hi.studio.msiha.data.entity.response

import hi.studio.msiha.data.entity.DetailAcceptance

class AcceptanceDetailResponse(
    val data: DetailAcceptance?
) {
}