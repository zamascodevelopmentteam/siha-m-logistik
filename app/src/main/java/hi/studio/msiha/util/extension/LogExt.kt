package hi.studio.hipechat.util.extension

import android.util.Log
import hi.studio.msiha.BuildConfig

/**
 * Created by mochadwi on 2019-11-03
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

fun logD(
    msg: String,
    tag: String? = null
) {
    if (BuildConfig.DEBUG)
        Log.d(tag ?: "HIPE", msg)
}
