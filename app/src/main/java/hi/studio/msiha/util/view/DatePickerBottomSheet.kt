package hi.studio.msiha.util.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import hi.studio.msiha.R
import hi.studio.hipechat.util.extension.textToDate
import kotlinx.android.synthetic.main.bottom_sheet_date_picker.*
import kotlinx.android.synthetic.main.bottom_sheet_date_picker.view.*
import kotlinx.android.synthetic.main.bottom_sheet_date_picker.view.datepickerdialog
import net.danlew.android.joda.JodaTimeAndroid
import org.joda.time.DateTime
import java.text.SimpleDateFormat
import java.util.*


interface DatePickerDialogBot{
    fun datePicker(dateToServer: String, dateToTextView: String)
}

class DatePickerBottomSheet(private val defaultDate:String,private val action: DatePickerDialogBot)
    : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottom_sheet_date_picker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        JodaTimeAndroid.init(activity)
        if(defaultDate!=""){
            val dateString = defaultDate.textToDate()
            val dates = dateString.split("-")
            datepickerdialog.updateDate(dates[0].toInt(),dates[1].toInt(),dates[2].toInt())
        }
        view.btnPickerDialogDone.setOnClickListener {
            getDate(view)
            dismiss()
        }
    }

    private fun getDate(view:View){
        val picker = view.datepickerdialog
        val c = Calendar.getInstance()
        val format = "yyyy-MM-dd"
        val localeID = Locale("in", "ID")
        val simpleDateFormat = SimpleDateFormat(format, localeID)

        c.set(Calendar.YEAR, picker.year)
        c.set(Calendar.MONTH, picker.month)
        c.set(Calendar.DAY_OF_MONTH, picker.dayOfMonth)

        action.datePicker(DateTime(c.time).toDateTimeISO().toString(),simpleDateFormat.format(c.time))

    }


}