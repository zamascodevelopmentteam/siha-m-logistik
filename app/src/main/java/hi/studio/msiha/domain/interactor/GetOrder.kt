package hi.studio.msiha.domain.interactor

import android.content.Context
import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.data.entity.request.AddOrderRequest
import hi.studio.msiha.data.entity.response.AvailableMedicineResponse
import hi.studio.msiha.data.entity.response.CityResponse
import hi.studio.msiha.data.entity.response.OrderActiveResponse
import hi.studio.msiha.domain.repository.OrderRepository
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.toSingleEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class GetOrder (private val orderRepository: OrderRepository, val context: Context): CoroutineScope {

    val allActiveOrderResult = MutableLiveData<Resource<OrderActiveResponse>>().toSingleEvent()
    val availableMedicine = MutableLiveData<Resource<AvailableMedicineResponse>>().toSingleEvent()
    val allCitiesResult = MutableLiveData<Resource<CityResponse>>().toSingleEvent()
    val sendOrderRequestResult = MutableLiveData<Resource<Void>>().toSingleEvent()
    val getAllRequestOut = MutableLiveData<Resource<OrderActiveResponse>>().toSingleEvent()


    override val coroutineContext: CoroutineContext = Dispatchers.IO


    fun sendOrderRequest(addOrderRequest: AddOrderRequest){
        val response = orderRepository.sendOrderRequest(addOrderRequest)
        sendOrderRequestResult.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if(data.isSuccessful){
                sendOrderRequestResult.postValue(Resource.success(null))
            }else{
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    sendOrderRequestResult.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    sendOrderRequestResult.postValue(Resource.error("Internal Server Error", null))
                }
            }
        }
    }

    fun getAllRequestOut(){
        val response = orderRepository.getAllActiveOrderOut()
        getAllRequestOut.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.code() != 200) {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    getAllRequestOut.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    getAllRequestOut.postValue(Resource.error("Internal Server Error", null))
                }
            } else {
                val rsp = data.body()
                getAllRequestOut.postValue(Resource.success(rsp))
            }
        }
    }


    fun getAllOrderActive(){
        val response = orderRepository.getAllActiveOrder()
        allActiveOrderResult.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.code() != 200) {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    allActiveOrderResult.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    allActiveOrderResult.postValue(Resource.error("Internal Server Error", null))
                }
            } else {
                val rsp = data.body()
                allActiveOrderResult.postValue(Resource.success(rsp))
            }
        }
    }

    fun getAvailMed(){
        val response = orderRepository.getAvailMed()
        availableMedicine.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.code() != 200) {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    availableMedicine.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    availableMedicine.postValue(Resource.error("Internal Server Error", null))
                }
            } else {
                val rsp = data.body()
                availableMedicine.postValue(Resource.success(rsp))
            }
        }
    }

    fun getCities(){
        val response = orderRepository.getAllCities()
        allCitiesResult.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.code() != 200) {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    allCitiesResult.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    allCitiesResult.postValue(Resource.error("Internal Server Error", null))
                }
            } else {
                val rsp = data.body()
                allCitiesResult.postValue(Resource.success(rsp))
            }
        }
    }
}