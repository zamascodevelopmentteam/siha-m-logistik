package hi.studio.msiha.data.entity.response

import hi.studio.msiha.data.entity.AvailableMedicine

data class AvailableMedicineResponse(
    val data: ArrayList<AvailableMedicine>
) {
}