/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.hipechat.util.extension


/**
 * Created by mochadwi on 2019-10-21
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

val String?.first: String
    get() = if (this.isNullOrBlank()) "$this" else "${this.first()}"

val CharSequence?.default: String
    get() = "$this"

val String?.default: String
    get() = this ?: ""

val Boolean?.default: Boolean
    get() = this ?: false

val Int?.default: Int
    get() = this ?: 0

val <T> Iterable<T>?.default: List<T>
    get() = this as List<T>? ?: emptyList()