package hi.studio.msiha.data.entity


data class OrderLogisticIn(
    val approvalNotes: String?,
    val approvalStatus: String?,
    val createdAt: String?,
    val createdBy: String?,
    val deletedAt: String?,
    val droppingNumber: String?,
    val entityName: String?,
    val fundSource: String?,
    val id: String,
    val logisticRole: String?,
    val notes: String?,
    val orderId: Int?,
    val picId: String?,
    val planStatus: String?,
    val provinceId: String?,
    val recieverName: String?,
    val senderName: String?,
    val sudinKabKotaId: String?,
    val updatedAt: String?,
    val updatedBy: Int?,
    val upkId: Int?
)