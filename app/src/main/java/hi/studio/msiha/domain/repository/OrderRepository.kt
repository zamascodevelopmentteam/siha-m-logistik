package hi.studio.msiha.domain.repository

import hi.studio.msiha.data.entity.request.AddOrderRequest
import hi.studio.msiha.data.entity.response.AvailableMedicineResponse
import hi.studio.msiha.data.entity.response.CityResponse
import hi.studio.msiha.data.entity.response.OrderActiveResponse
import hi.studio.msiha.data.entity.response.OrderDetailResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response

interface OrderRepository {
    fun getAllActiveOrder(): Deferred<Response<OrderActiveResponse>>
    fun getAllActiveOrderOut(): Deferred<Response<OrderActiveResponse>>
    fun getAvailMed(): Deferred<Response<AvailableMedicineResponse>>
    fun getAllCities(): Deferred<Response<CityResponse>>
    fun sendOrderRequest(addOrderRequest: AddOrderRequest):Deferred<Response<Void>>
    fun getDetailRequest(id: String): Deferred<Response<OrderDetailResponse>>
}