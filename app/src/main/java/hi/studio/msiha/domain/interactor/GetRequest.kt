package hi.studio.msiha.domain.interactor

import android.content.Context
import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.data.entity.response.OrderActiveResponse
import hi.studio.msiha.data.entity.response.OrderDetail
import hi.studio.msiha.domain.repository.OrderRepository
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.toSingleEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class GetRequest(private val requestRepository: OrderRepository, val context: Context):
    CoroutineScope {

    val getAllRequest = MutableLiveData<Resource<OrderActiveResponse>>().toSingleEvent()
    val getAllRequestOut = MutableLiveData<Resource<OrderActiveResponse>>().toSingleEvent()
    val getAllDetailRequest = MutableLiveData<Resource<OrderDetail>>().toSingleEvent()

    override val coroutineContext: CoroutineContext = Dispatchers.IO

    fun getAllRequest(){
        val response = requestRepository.getAllActiveOrder()
        getAllRequest.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.code() != 200) {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    getAllRequest.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    getAllRequest.postValue(Resource.error("Internal Server Error", null))
                }
            } else {
                val rsp = data.body()
                getAllRequest.postValue(Resource.success(rsp))
            }
        }
    }

    fun getAllRequestOut(){
        val response = requestRepository.getAllActiveOrderOut()
        getAllRequestOut.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.code() != 200) {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    getAllRequestOut.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    getAllRequestOut.postValue(Resource.error("Internal Server Error", null))
                }
            } else {
                val rsp = data.body()
                getAllRequestOut.postValue(Resource.success(rsp))
            }
        }
    }


    fun getRequestDetail(id: String){
        val response = requestRepository.getDetailRequest(id)
        getAllDetailRequest.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.code() != 200) {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    getAllDetailRequest.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    getAllDetailRequest.postValue(Resource.error("Internal Server Error", null))
                }
            } else {
                val rsp = data.body()
                getAllDetailRequest.postValue(Resource.success(rsp?.data))
            }
        }
    }
}