package hi.studio.msiha.ui.main.request

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.entity.response.User
import hi.studio.msiha.data.source.local.contentprovider.USER

class RequestMainAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
    override fun getCount(): Int {
        if ((Hawk.get(USER) as User).logisticrole.toString() == "MINISTRY_ENTITY"){
            return 1
        }else{
            return 2
        }
    }

    override fun getItem(position: Int): Fragment {
        return when (position){
            0 ->RequestInFragment()
            1 ->RequestOutFragment()
            else -> RequestOutFragment()
        }
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Masuk"
            1 -> "Keluar"
            else ->"Masuk"
        }
    }
}