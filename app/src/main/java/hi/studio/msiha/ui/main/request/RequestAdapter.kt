package hi.studio.msiha.ui.main.request

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.OrderActive
import hi.studio.msiha.data.source.local.contentprovider.APPROVED_INTERNAL
import hi.studio.msiha.data.source.local.contentprovider.EXECUTED
import kotlinx.android.synthetic.main.list_request.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
//RTL8723BE
interface OnItemClick{
    fun onItemClicked(order: OrderActive)
}

class RequestAdapter(private var order: List<OrderActive>,
                     private var type: Int,
                     private val onItemClick: OnItemClick
): RecyclerView.Adapter<RequestAdapter.MainViewHolder>(), Filterable {

    private var filterOrder = order

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_request, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return filterOrder.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val currentOrder = filterOrder[position]
        val format2 = "yyyy-MM-dd"
        val localeID = Locale("in", "ID")
        val simpleDateFormat2 = SimpleDateFormat(format2, localeID)
        val date = simpleDateFormat2.parse(currentOrder.createdAt)
        val expired = simpleDateFormat2.format(date)

        when(type){
            0 -> holder.inteded.text = "Ditujukan : "+currentOrder.senderName
            1 -> holder.inteded.text = "Pengaju: "+currentOrder.recieverName
        }

        var approvalStatus = currentOrder.lastApprovalStatus
        when (approvalStatus) {
            "APPROVED_FULLY" -> approvalStatus = "Diterima Penuh"
            "APPROVED_FULL" -> approvalStatus = "Diterima Penuh"
            "APPROVED_DIFFERENT" -> approvalStatus = "Diterima Sebagian"
            "CONFIRMED" -> approvalStatus = "Telah Dikonfirmasi"
            "IN_EVALUATION" -> approvalStatus = "Dieavaluasi"
            "EVALUATION" -> approvalStatus = "Dievaluasi"
            EXECUTED -> approvalStatus = "Diterima"
            "DRAFT" -> approvalStatus = "Draft"
            "PROCESSED" -> approvalStatus = "Diproses"
        }

        var accStatus = currentOrder.lastOrderStatus
        when (accStatus) {
            "APPROVED_FULLY" -> accStatus = "Diterima Penuh"
            "APPROVED_FULL" -> accStatus = "Diterima Penuh"
            "APPROVED_DIFFERENT" -> accStatus = "Diterima Sebagian"
            "CONFIRMED" -> accStatus = "Telah Dikonfirmasi"
            "IN_EVALUATION" -> accStatus = "Dieavaluasi"
            EXECUTED -> approvalStatus = "Diterima"
            APPROVED_INTERNAL -> approvalStatus = "Disetujui"
            "EVALUATION" -> accStatus = "Dievaluasi"
            "DRAFT" -> accStatus = "Draft"
            "PROCESSED" -> accStatus = "Diproses"
        }

        holder.invNum.text = currentOrder.orderCode
        holder.statusAcc.text = "Status Penerimaan: "+ accStatus
        holder.statusApproval.text = "Status Approval: "+ approvalStatus
        holder.dateMakeOrder.text = expired

        holder.wrapper.setOnClickListener {
            onItemClick.onItemClicked(currentOrder)
        }
    }

    inner class MainViewHolder(val view: View)  : RecyclerView.ViewHolder(view){
        val invNum = view.tv_no_inv_order
        val statusAcc = view.tv_status_accept
        val statusApproval = view.tv_status_approval
        val dateMakeOrder = view.tv_date_make_order
        val inteded = view.tv_intended
        val wrapper = view.wrapperListRequest
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            @SuppressLint("LogNotTimber")
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val charSearch: String = p0.toString()
                if (charSearch.isEmpty()) {
                    filterOrder = order
                } else {
                    val resultList = ArrayList<OrderActive>()
                    for (row in order) {
                        if(row.orderCode!!.toLowerCase().contains(charSearch.toLowerCase())) resultList.add(row)
                    }
                    filterOrder = resultList
                }
                val filterResult = FilterResults()
                filterResult.values = filterOrder
                return filterResult
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                filterOrder = p1?.values as ArrayList<OrderActive>
                notifyDataSetChanged()
            }
        }
    }

}