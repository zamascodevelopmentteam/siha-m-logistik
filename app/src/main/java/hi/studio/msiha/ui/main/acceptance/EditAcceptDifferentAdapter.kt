package hi.studio.msiha.ui.main.acceptance

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import hi.studio.msiha.data.entity.DistributionPlanItems
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.AcceptanceDifferent
import hi.studio.msiha.data.entity.request.DistributionPlans
import kotlinx.android.synthetic.main.list_edit_different.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class EditAcceptDifferentAdapter(
    val itemAcceptance: List<DistributionPlanItems>
) :
    RecyclerView.Adapter<EditAcceptDifferentAdapter.MainViewHolder>() {

    private var selectedItem = mutableSetOf<AcceptanceDifferent>()
    private val selectedId = mutableSetOf<Int>()
    private lateinit var listData: ArrayList<DistributionPlans>

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainViewHolder {
        return  MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_edit_different, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return itemAcceptance.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val items = itemAcceptance[position]
        if (!items.expiredDate.isNullOrEmpty()) {
            val format2 = "yyyy-MM-dd"
            val localeID = Locale("in", "ID")
            val simpleDateFormat2 = SimpleDateFormat(format2, localeID)
            val date = simpleDateFormat2.parse(items.expiredDate)
            holder.expiredDate.text = "Expired Date: ${simpleDateFormat2.format(date)}"
        }

        holder.batchCode.text = "Batch Code: ${items.inventory?.batchCode}"
        holder.medName.text = items.medicine?.name
        holder.batchCode.text = items.inventory?.batchCode
        listData = ArrayList()

        itemAcceptance.forEach {
            listData.add(
                DistributionPlans(
                    id = it.distPlanItemId,
                    actualReceivedPackageQuantity = 1
                )
            )
        }

        holder.qty.afterTextChanged { input ->
            if(input!="") itemAcceptance[position].packageQuantity=input.toInt()
        }
    }

    fun getItem():List<DistributionPlanItems>{
        return itemAcceptance
    }

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val medName = view.textMedName1
        val batchCode = view.tvBatchCode
        val expiredDate = view.textMedExpiredDate1
        val qty = view.editTextQty
    }

}


private fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            //afterTextChanged.invoke(p0.toString())
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            afterTextChanged.invoke(p0.toString())
        }

    })
}
