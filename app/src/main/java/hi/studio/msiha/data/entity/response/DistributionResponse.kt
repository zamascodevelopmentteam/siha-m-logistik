package hi.studio.msiha.data.entity.response

import hi.studio.msiha.data.entity.DistributionItems

class DistributionResponse (
    val data: List<DistributionItems>
)