package hi.studio.msiha.data.entity.response

class PickingListResponse(
    val inventoryId: String,
    val batchCode: String,
    val brandId: Int,
    val brandName: String,
    val expiredDate:  String,
    val packageUnitType: String,
    val packageQuantity: String
)