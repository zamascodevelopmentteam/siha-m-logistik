package hi.studio.msiha.util.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import hi.studio.msiha.R
import hi.studio.hipechat.util.extension.textToImageEncode
import kotlinx.android.synthetic.main.bottom_sheet_view_qr.*

class QrCodeBottomSheet(private val id: String?) : BottomSheetDialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.bottom_sheet_view_qr, container, false)
        dialog!!.setCanceledOnTouchOutside(false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnClose.setOnClickListener {
            this.dismiss()
        }
        if (id == null) {
            val ids = "invalid id"
            imgQr.setImageBitmap(ids.textToImageEncode())

        } else {
            imgQr.setImageBitmap(id?.textToImageEncode())
            textNoSuratJalan.text = id
        }

    }
}
