package hi.studio.msiha.data.entity

import com.google.gson.annotations.SerializedName

data class AvailableMedicine(
    val medicineId:Int?,
    @SerializedName("medicinename")
    val medicineName: String?,
    @SerializedName("packageunit")
    val packageUnit: String?,
    @SerializedName("packagequantity")
    val packageQuantity: Int?,
    var expiredDate:String?,
    var requestedQuantity:Int?
)