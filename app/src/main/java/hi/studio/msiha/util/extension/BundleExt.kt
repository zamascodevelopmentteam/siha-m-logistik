/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.hipechat.util.extension

import android.os.Binder
import android.os.Bundle
import android.os.Parcelable
import android.util.Size
import android.util.SizeF
import android.util.SparseArray
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import java.io.Serializable

/**
 * Created by mochadwi on 2019-10-28
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

// Usage e.g: https://medium.com/@programmerr47/creating-bundles-nicely-with-kotlin-1526fc173d01
class BundlePair(
    private val block: (Bundle) -> Unit
) {
    fun apply(bundle: Bundle) = block(bundle)
}

inline fun bundle(initFun: Bundle.() -> Unit) = Bundle().apply(initFun)
inline fun bundleOf(initFun: (Bundle) -> Unit) =
    Bundle().also(initFun)

fun bundleOf(vararg pairs: BundlePair) = bundle {
    pairs.forEach { it.apply(this) }
}

infix fun String.bundleTo(value: Boolean) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Byte) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Short) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Int) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Long) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Float) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Double) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Char) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: CharSequence) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: String) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Bundle) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Parcelable) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Serializable) = BundlePair { it[this] = value }

@RequiresApi(18)
infix fun String.bundleTo(value: Binder) = BundlePair { it[this] = value }

@RequiresApi(21)
infix fun String.bundleTo(value: Size) = BundlePair { it[this] = value }

@RequiresApi(21)
infix fun String.bundleTo(value: SizeF) = BundlePair { it[this] = value }

infix fun String.bundleTo(value: BooleanArray) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: ByteArray) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: ShortArray) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: IntArray) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: LongArray) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: FloatArray) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: DoubleArray) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: CharArray) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Array<out CharSequence>) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Array<out String>) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: Array<out Parcelable>) = BundlePair { it[this] = value }
infix fun String.bundleTo(value: ArrayList<out Parcelable>) = BundlePair { it[this] = value }

infix fun String.bundleTo(value: SparseArray<out Parcelable>) = BundlePair { it[this] = value }

//for null
infix fun String.bundleTo(n: Void?) = BundlePair { it[this] = null }


operator fun Bundle.set(key: String, value: Boolean) = putBoolean(key, value)
operator fun Bundle.set(key: String, value: Byte) = putByte(key, value)
operator fun Bundle.set(key: String, value: Short) = putShort(key, value)
operator fun Bundle.set(key: String, value: Int) = putInt(key, value)
operator fun Bundle.set(key: String, value: Long) = putLong(key, value)
operator fun Bundle.set(key: String, value: Float) = putFloat(key, value)
operator fun Bundle.set(key: String, value: Double) = putDouble(key, value)
operator fun Bundle.set(key: String, value: Char) = putChar(key, value)
operator fun Bundle.set(key: String, value: CharSequence) = putCharSequence(key, value)
operator fun Bundle.set(key: String, value: String) = putString(key, value)
operator fun Bundle.set(key: String, value: Bundle) = putBundle(key, value)
operator fun Bundle.set(key: String, value: Parcelable) = putParcelable(key, value)
operator fun Bundle.set(key: String, value: Serializable) = putSerializable(key, value)

@RequiresApi(18)
operator fun Bundle.set(key: String, value: Binder) = putBinder(key, value)

@RequiresApi(21)
operator fun Bundle.set(key: String, value: Size) = putSize(key, value)

@RequiresApi(21)
operator fun Bundle.set(key: String, value: SizeF) = putSizeF(key, value)

operator fun Bundle.set(key: String, value: BooleanArray) = putBooleanArray(key, value)
operator fun Bundle.set(key: String, value: ByteArray) = putByteArray(key, value)
operator fun Bundle.set(key: String, value: ShortArray) = putShortArray(key, value)
operator fun Bundle.set(key: String, value: IntArray) = putIntArray(key, value)
operator fun Bundle.set(key: String, value: LongArray) = putLongArray(key, value)
operator fun Bundle.set(key: String, value: FloatArray) = putFloatArray(key, value)
operator fun Bundle.set(key: String, value: DoubleArray) = putDoubleArray(key, value)
operator fun Bundle.set(key: String, value: CharArray) = putCharArray(key, value)
operator fun Bundle.set(key: String, value: Array<out CharSequence>) =
    putCharSequenceArray(key, value)

operator fun Bundle.set(key: String, value: Array<out String>) = putStringArray(key, value)
operator fun Bundle.set(key: String, value: Array<out Parcelable>) = putParcelableArray(key, value)
operator fun Bundle.set(key: String, value: ArrayList<out Parcelable>) =
    putParcelableArrayList(key, value)

operator fun Bundle.set(key: String, value: SparseArray<out Parcelable>) =
    putSparseParcelableArray(key, value)

//for null
object Null

infix fun String.bundleTo(value: Null) = BundlePair { it[this] = value }

fun String.bundleToNull() = BundlePair { it.putNull(this) }
fun Bundle.putNull(key: String) = putString(key, null)

operator fun Bundle.set(key: String, value: Null) = putNull(key)
operator fun Bundle.set(key: String, value: Void?) = putString(key, null)

// Usage e.g: https://medium.com/@programmerr47/kotlin-operation-scopes-f2a7c20aba9c
interface BundleScope {
    operator fun String.minus(value: Boolean) = this bundleTo value
    operator fun String.minus(value: Byte) = this bundleTo value
    operator fun String.minus(value: Short) = this bundleTo value
    operator fun String.minus(value: Int) = this bundleTo value
    operator fun String.minus(value: Long) = this bundleTo value
    operator fun String.minus(value: Float) = this bundleTo value
    operator fun String.minus(value: Double) = this bundleTo value
    operator fun String.minus(value: Char) = this bundleTo value
    operator fun String.minus(value: CharSequence) = this bundleTo value
    operator fun String.minus(value: String) = this bundleTo value
    operator fun String.minus(value: Bundle) = this bundleTo value
    operator fun String.minus(value: Parcelable) = this bundleTo value
    operator fun String.minus(value: Serializable) = this bundleTo value

    @RequiresApi(18)
    operator fun String.minus(value: Binder) = this bundleTo value

    @RequiresApi(21)
    operator fun String.minus(value: Size) = this bundleTo value

    @RequiresApi(21)
    operator fun String.minus(value: SizeF) = this bundleTo value

    operator fun String.minus(value: BooleanArray) = this bundleTo value
    operator fun String.minus(value: ByteArray) = this bundleTo value
    operator fun String.minus(value: ShortArray) = this bundleTo value
    operator fun String.minus(value: IntArray) = this bundleTo value
    operator fun String.minus(value: LongArray) = this bundleTo value
    operator fun String.minus(value: FloatArray) = this bundleTo value
    operator fun String.minus(value: DoubleArray) = this bundleTo value
    operator fun String.minus(value: CharArray) = this bundleTo value
    operator fun String.minus(value: Array<out CharSequence>) = this bundleTo value
    operator fun String.minus(value: Array<out String>) = this bundleTo value
    operator fun String.minus(value: Array<out Parcelable>) = this bundleTo value
    operator fun String.minus(value: ArrayList<out Parcelable>) = this bundleTo value

    operator fun String.minus(value: SparseArray<out Parcelable>) = this bundleTo value

    operator fun String.minus(value: Null) = this.bundleToNull()
}

object LocalBundleScope : BundleScope

inline fun inContentValuesScope(block: LocalBundleScope.() -> Unit) =
    LocalBundleScope.run(block)

inline fun <R> inBundleScope(block: LocalBundleScope.() -> R) =
    LocalBundleScope.run(block)

fun <FRAGMENT : Fragment> FRAGMENT.putArgs(vararg pairs: BundlePair): FRAGMENT =
    apply { arguments = bundleOf(*pairs) }
