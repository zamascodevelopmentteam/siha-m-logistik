package hi.studio.msiha.data.entity.response

import hi.studio.msiha.data.entity.OrderLogisticIn

data class OrdersInResponse(
    val data: List<OrderLogisticIn>
)