package hi.studio.msiha.data.source.network

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import hi.studio.msiha.BuildConfig
import hi.studio.msiha.HiStudioApp
import hi.studio.msiha.data.source.local.contentprovider.API_URL
import hi.studio.hipechat.util.LiveDataCallAdapterFactory
import okhttp3.Authenticator
import okhttp3.Dispatcher
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

object ApiService {

    inline fun <reified T> apiServiceGeneric(
        context: Context,
        gson: Gson,
        userAgent: String,
        auth: Authenticator?
    ): T {
        val app = context.applicationContext as HiStudioApp

        // timeout (second)
        var CONNECT_TIMEOUT: Long = 30
        var WRITE_TIMEOUT: Long = 30
        var READ_TIMEOUT: Long = 30

        if (BuildConfig.DEBUG) {
            CONNECT_TIMEOUT = 5
            WRITE_TIMEOUT = 10
            READ_TIMEOUT = 10
        }

        val PROTOCOLS = listOf(Protocol.HTTP_1_1, Protocol.HTTP_2)
        val MAX_REQUEST_PER_HOST = 500

        val endpoint = API_URL
        val dispatcher = Dispatcher()
        dispatcher.maxRequestsPerHost = MAX_REQUEST_PER_HOST

        // logger
        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Timber.d(it)
        })
        logger.level = HttpLoggingInterceptor.Level.BODY

        // network state interceptor
        val interceptorNetwork = object : NetworkConnectionInterceptor() {
            override fun onConnectBackendAPIFailed() {
                Timber.d("<<< onResolveDomain failed")
                app.mInternetConnectionListener?.onConnectAPIFailed()
            }

            override fun isInternetAvailable(): Boolean {
                return isNetworkAvailable(context)
            }

            override fun onInternetUnavailable() {
                Timber.d("<<< onInternetUnavailable failed ${app.mInternetConnectionListener}")
                app.mInternetConnectionListener?.onInternetUnavailable()
            }

        }

        // http client
        val client = UnsafeOkHttpClient.unsafeOkHttpClient
        //val client = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val originalRequest = chain.request()
                val newRequest = originalRequest
                    .newBuilder()
                    .header("Accept", "application/vnd.full+json")
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/vnd.full+json")
                    .header("Content-Type", "application/json")
                    .method(originalRequest.method(), originalRequest.body())
                    .build()
                val response = chain.proceed(newRequest)
                response
            }
            .apply { if (auth != null) authenticator(auth) }
            .addInterceptor(interceptorNetwork)
            .addInterceptor(logger)
            .addInterceptor(ChuckerInterceptor(context))
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .dispatcher(dispatcher)
            .retryOnConnectionFailure(true)
            .protocols(PROTOCOLS)
            .build()

        // retrofit client
        val retrofit = Retrofit.Builder()
            .baseUrl(endpoint)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(client)
            .build()

        return retrofit.create(T::class.java)
    }

    fun apiService(
        context: Context,
        gson: Gson,
        userAgent: String
        //auth: Authenticator?
    ): Endpoint {
        val app = context.applicationContext as HiStudioApp

        // timeout (second)
        var CONNECT_TIMEOUT: Long = 30
        var WRITE_TIMEOUT: Long = 30
        var READ_TIMEOUT: Long = 30

        if (BuildConfig.DEBUG) {
            CONNECT_TIMEOUT = 5
            WRITE_TIMEOUT = 10
            READ_TIMEOUT = 10
        }

        val PROTOCOLS = listOf(Protocol.HTTP_1_1, Protocol.HTTP_2)
        val MAX_REQUEST_PER_HOST = 500

        val endpoint = API_URL
        val dispatcher = Dispatcher()
        dispatcher.maxRequestsPerHost = MAX_REQUEST_PER_HOST

        // logger
        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Timber.d(it)
        })
        logger.level = HttpLoggingInterceptor.Level.BODY

        // network state interceptor
        val interceptorNetwork = object : NetworkConnectionInterceptor() {
            override fun onConnectBackendAPIFailed() {
                Timber.d("<<< onResolveDomain failed")
                app.mInternetConnectionListener?.onConnectAPIFailed()
            }

            override fun isInternetAvailable(): Boolean {
                return isNetworkAvailable(context)
            }

            override fun onInternetUnavailable() {
                Timber.d("<<< onInternetUnavailable failed ${app.mInternetConnectionListener}")
                app.mInternetConnectionListener?.onInternetUnavailable()
            }

        }


        // http client
        val client = UnsafeOkHttpClient.unsafeOkHttpClient
            // val client = OkHttpClient.Builder()
            //.apply { if (auth != null) authenticator(auth) }
            .addInterceptor(StethoInterceptor())
            .addInterceptor(interceptorNetwork)
            .addInterceptor(ChuckerInterceptor(context))
            .addInterceptor(logger)
            .addInterceptor { chain ->
                val originalRequest = chain.request()
                val newRequest = originalRequest
                    .newBuilder()
                    .header("Accept", "application/vnd.full+json")
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/vnd.full+json")
                    .header("Content-Type", "application/json")
                    .method(originalRequest.method(), originalRequest.body())
                    .build()
                val response = chain.proceed(newRequest)
                response
            }
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .dispatcher(dispatcher)
            .retryOnConnectionFailure(true)
            .protocols(PROTOCOLS)
            .build()

        // retrofit client
        val retrofit = Retrofit.Builder()
            .baseUrl(endpoint)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(client)
            .build()

        return retrofit.create(Endpoint::class.java)
    }
}