package hi.studio.msiha.ui.main.dashboard


import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.AvailableMedicine
import hi.studio.msiha.data.entity.response.AvailableMedicineResponse
import hi.studio.msiha.ui.main.RequestMedicine
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.fragment_list_item.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListItemFragment : Fragment(),
    AdapterAction,
    ConfirmAddMed {

    private lateinit var listItemAdapter: ListItemAdapter
    private val viewModel: DashboardViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_item, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_search, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()
        viewModel.getAvailMed()
    }

    override fun onResume() {
        super.onResume()
        observeLiveData()
    }

    override fun onItemClick(med: AvailableMedicine) {
        val bottomSheetAddMedicine = BottomSheetAddMedicine(med,this)
        bottomSheetAddMedicine.show(activity!!.supportFragmentManager, "tambah_obat")
    }

    override fun addMed(medicine: AvailableMedicine) {
        (activity as RequestMedicine).addMedicine(medicine)
        findNavController().navigateUp()
    }

    private fun handleMedList(orderList2: ArrayList<AvailableMedicine>){
        context?.let {
            listItemAdapter = ListItemAdapter(orderList2, it,this)

            rvListMedicine.apply {
                adapter = listItemAdapter
                layoutManager = LinearLayoutManager(activity) as RecyclerView.LayoutManager?
            }
        }
    }



    private fun observeLiveData(){
        viewModel.availMedResult.observe(this, Observer<Resource<AvailableMedicineResponse>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        handleMedList(it.data.data)
                    } else {
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> {
                }
            }
        })
    }

}
