package hi.studio.msiha

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import hi.studio.msiha.data.repository.*
import hi.studio.msiha.data.source.network.ApiService
import hi.studio.msiha.domain.interactor.*
import hi.studio.msiha.domain.repository.*
import hi.studio.msiha.ui.auth.signin.SignInViewModel
import hi.studio.msiha.ui.main.acceptance.AcceptanceViewModel
import hi.studio.msiha.ui.main.dashboard.DashboardViewModel
import hi.studio.msiha.ui.main.distribution.DistributionViewModel
import hi.studio.msiha.ui.main.request.RequestViewModel
import hi.studio.hipechat.util.DateTimeTypeConverter
import org.joda.time.DateTime
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val hiStudioModule = module {
    single { ApiService.apiService(androidContext(), get(), androidContext().packageName) }

    single<Gson>{
        GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .registerTypeAdapter(DateTime::class.java, DateTimeTypeConverter())
            .create()
    }
    factory { AuthRepositoryImpl(get()) as AuthRepository }
    single { MainRepositoryImpl(get()) as MainRepository }
    single { AcceptanceRepositoryImpl(get()) as AcceptanceRepository}
    single { OrderRepositoryImpl(get()) as OrderRepository }
    single { RequestRepositoryImpl(get()) as RequestRepository }
    single { DistributionRepositoryImpl(get()) as DistributionRepository }

    single { GetAuth(get(), androidContext()) }
    single { GetOrder(get(), androidContext()) }
    single { GetMain(get(), androidContext()) }
    single { GetAcceptance(get(), androidContext()) }
    single { GetRequest(get(), androidContext()) }
    single { GetDistribution(get(), androidContext()) }

    viewModel { SignInViewModel(get()) }
    viewModel { DashboardViewModel(get()) }
    viewModel { RequestViewModel(get()) }
    viewModel { AcceptanceViewModel(get())}
    viewModel { DistributionViewModel(get())}

}