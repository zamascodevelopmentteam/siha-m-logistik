package hi.studio.msiha.domain.repository

import hi.studio.msiha.data.entity.response.OrderActiveResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response

interface RequestRepository {
    fun getAllActiveOrder(): Deferred<Response<OrderActiveResponse>>
}