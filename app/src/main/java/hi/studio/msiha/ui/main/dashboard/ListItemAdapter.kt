package hi.studio.msiha.ui.main.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.AvailableMedicine
import kotlinx.android.synthetic.main.list_items.view.*


interface AdapterAction{
    fun onItemClick(med:AvailableMedicine)
}

class ListItemAdapter(
    private var availMedicine: List<AvailableMedicine>,
    private val context: Context,
    private val action:AdapterAction
) : RecyclerView.Adapter<ListItemAdapter.MainViewHolder>() {

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val medName = view.item_name
        val medQty = view.item_stock
        val buttonChoose = view.choosen_item
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_items, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return availMedicine.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val current = availMedicine[position]
        holder.medName.text = current.medicineName
        holder.medQty.text = "Stok: " + current.packageQuantity.toString() + " " + current.packageUnit
        holder.buttonChoose.setOnClickListener {
            action.onItemClick(current)
        }
    }
}