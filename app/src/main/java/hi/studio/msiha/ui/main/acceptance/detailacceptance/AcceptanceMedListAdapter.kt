package hi.studio.msiha.ui.main.acceptance.detailacceptance

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.DistributionPlanItems
import kotlinx.android.synthetic.main.item_detail_accept.view.*
import java.text.SimpleDateFormat
import java.util.*

class AcceptanceMedListAdapter(val distPlan: List<DistributionPlanItems>) :
    RecyclerView.Adapter<AcceptanceMedListAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_detail_accept, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return distPlan.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val dist = distPlan[position]
        var expired: String? = "0000"
        val format2 = "yyyy-MM-dd"
        var date: Date? = Date()
        val localeID = Locale("in", "ID")
        val simpleDateFormat2 = SimpleDateFormat(format2, localeID)

        if (dist.expiredDate != null) {
            date = simpleDateFormat2.parse(dist.expiredDate)
            expired = simpleDateFormat2.format(date)
            holder.textExpiredDate.text = "Expired Date: $expired"
        }
        if (dist.medicine != null) {
            holder.textMedName.text = dist.medicine.name
            holder.textJenisBarang.text = "Jenis Barang: ${dist.medicine.codeName}"
        }
        if (dist.packageQuantity != null || dist.packageUnitType != null) {
            holder.textJumlahBarang.text =
                "Jumlah Barang: ${dist.packageQuantity} ${dist.packageUnitType}"
        }
    }

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var textMedName = view.textMedNameAcc
        var textJenisBarang = view.textJenisBarangAcc
        var textJumlahBarang = view.textJumlahBarangAcc
//        var textTanggalMasuk = view.textTanggalMasukAcc
        var textExpiredDate = view.textExpiredDateAcc
    }
}