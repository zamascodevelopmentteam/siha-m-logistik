package hi.studio.msiha.ui.main.acceptance


import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.zxing.Result

import hi.studio.msiha.R
import kotlinx.android.synthetic.main.fragment_scanqr.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import android.Manifest
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.data.entity.request.SearchByQrCodeRequest
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import org.koin.androidx.viewmodel.ext.android.viewModel


/**
 * A simple [Fragment] subclass.
 */
class ScanqrFragment : Fragment(), ZXingScannerView.ResultHandler {

    private lateinit var scannerView: ZXingScannerView
    private val viewModel: AcceptanceViewModel by viewModel()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_scanqr, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleCamera()
        initDefaultView()
        observeLiveData()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private fun handleCamera() {
        scannerView = ZXingScannerView(activity)
        scannerView.setAutoFocus(true)
        scannerView.setResultHandler(this)
        frame_layout_camera.addView(scannerView)
    }

    override fun handleResult(rawResult: Result?) {
        text_view_qr_code_value.text = rawResult?.text
        viewModel.getQrCodeById(SearchByQrCodeRequest(rawResult?.text))
        observeLiveData()
        button_reset.visibility = View.INVISIBLE
    }

    override fun onStart() {
        super.onStart()
        scannerView.startCamera()
        doRequestPermission()
    }

    private fun doRequestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity?.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 100)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        observeLiveData()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            100 -> {
                handleCamera()
            }
            else -> {
                /* nothing to do in here */
            }
        }
    }

    private fun initDefaultView() {
        text_view_qr_code_value.text = "Dropping Number"
        button_reset.visibility = View.GONE
    }

    private fun observeLiveData() {
        viewModel.getQrCodeResult.observe(
            this,
            Observer<Resource<AcceptanceDetailResponse>?> {
                when (it?.status) {
                    Status.SUCCESS -> {
                        if (it.data?.data != null) {
                            val bundle = Bundle()
                            bundle.putString("id", it.data.data.id)
                            findNavController().navigate(R.id.detailAcceptanceFragment, bundle)
                        }else{
                            Toast.makeText(context, "Dropping Number tidak ditemukan", Toast.LENGTH_SHORT).show()
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {

                    }
                }
            })
    }


}
