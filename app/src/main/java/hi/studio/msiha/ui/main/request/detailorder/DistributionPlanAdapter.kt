package hi.studio.msiha.ui.main.request.detailorder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.DistributionItems
import hi.studio.msiha.data.source.local.contentprovider.*
import hi.studio.msiha.ui.main.distribution.OnDetailClick
import kotlinx.android.synthetic.main.list_distribution_in_detail_order.view.*
import java.text.SimpleDateFormat
import java.util.*

class DistributionPlanAdapter (private val items: List<DistributionItems>,
                               private val action: OnDetailClick) :
    RecyclerView.Adapter<DistributionPlanAdapter.MainViewHolder>() {

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var textMedName = view.tv_no_inv_dist1
        var lastUpdate = view.tv_last_update_dist
        var status = view.tv_status_dist
        var wrapper = view.wrapperDistPlan
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_distribution_in_detail_order, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val current = items[position]
        val format2 = "yyyy-MM-dd"
        val localeID = Locale("in", "ID")
        val simpleDateFormat2 = SimpleDateFormat(format2, localeID)
        val date = simpleDateFormat2.parse(current.updatedAt)
        val expired = simpleDateFormat2.format(date)

        holder.textMedName.text = current.droppingNumber

        var planStatus = current.planStatus
        when (planStatus) {
            EXECUTED -> planStatus = "Diterima"
            DRAFT -> planStatus = "Draft"
            PROCESSED -> planStatus = "Dalam Proses"
            EVALUATION -> planStatus = "Dalam Evaluasi"
            ACCEPTED_FULLY -> planStatus = "Diterima Penuh"
            ACCEPTED_DIFFERENT -> planStatus = "Diterima Sebagian"
            LOST -> planStatus = "Hilang"
            REJECTED -> planStatus = "Ditolah"
        }

        
        holder.status.text = planStatus
        holder.lastUpdate.text = "Expired Data: $expired"
//        holder.textBatchCode.text = current.createdAt

        holder.wrapper.setOnClickListener {
            action.onClick(current)
        }
    }
}