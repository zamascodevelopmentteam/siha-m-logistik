package hi.studio.msiha.ui.main.dashboard


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.AvailableMedicine
import hi.studio.msiha.ui.main.RequestMedicine
import hi.studio.msiha.ui.main.request.detailorder.CityIntendedBottomSheet
import kotlinx.android.synthetic.main.fragment_add_request.*
import kotlinx.android.synthetic.main.fragment_add_request.view.*
import androidx.recyclerview.widget.DividerItemDecoration
import hi.studio.msiha.data.entity.City
import hi.studio.msiha.data.entity.request.AddOrderRequest
import hi.studio.msiha.data.entity.response.CityResponse
import hi.studio.msiha.ui.main.request.detailorder.GetSelectedCity
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import org.koin.androidx.viewmodel.ext.android.viewModel


/**
 * A simple [Fragment] subclass.
 */
class AddRequestFragment : Fragment(),
    RequestMedicineAction,
    ConfirmAddMed,
    GetSelectedCity{

    private lateinit var adapterMedicine:RequestedMedicineAdapter
    private val viewModel: DashboardViewModel by viewModel()
    private var selectedCity = City()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        observeLiveData()
        return inflater.inflate(R.layout.fragment_add_request, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClick(view)
        setupRequestedMedicineList()
        viewModel.getCities()
    }

    override fun onEditMedicine(med: AvailableMedicine) {
        val bottomSheetAddMedicine = BottomSheetAddMedicine(med,this)
        bottomSheetAddMedicine.show(activity!!.supportFragmentManager, "edit_obat")
    }

    override fun addMed(medicine: AvailableMedicine) {
        (activity as RequestMedicine).addMedicine(medicine)
        adapterMedicine.updateMedicine((activity as RequestMedicine).getRequestedMedicine())

        if((activity as RequestMedicine).getRequestedMedicine().size==0){
            showEmptyState()
        }else{
            hideEmptyState()
        }
    }

    override fun getSelectedCity(city: City) {
        selectedCity=city
        tvcityIntended.text=city.name
    }

    private fun hideEmptyState(){
        recyclerViewRequestedMedicine.visibility=View.VISIBLE
        textEmpty.visibility=View.INVISIBLE
        buttonConfirm.isEnabled=true
        buttonConfirm.setBackgroundResource(R.drawable.bg_rounded_primary)
    }

    private fun showEmptyState(){
        recyclerViewRequestedMedicine.visibility=View.INVISIBLE
        textEmpty.visibility=View.VISIBLE
        buttonConfirm.isEnabled=false
        buttonConfirm.setBackgroundResource(R.drawable.bg_rounded_disable)
    }

    private fun setupRequestedMedicineList(){
        val requestedList = (activity as RequestMedicine).getRequestedMedicine()
        if(requestedList.size>0) {
            hideEmptyState()
            adapterMedicine = RequestedMedicineAdapter(requestedList, this)
            val dividerItemDecoration = DividerItemDecoration(
                recyclerViewRequestedMedicine.context,
                DividerItemDecoration.HORIZONTAL
            )
            recyclerViewRequestedMedicine.apply {
                adapter = adapterMedicine
                layoutManager = LinearLayoutManager(activity)
                addItemDecoration(dividerItemDecoration)
            }
        }else{
            showEmptyState()
        }
    }

    private fun observeLiveData(){
        viewModel.allCities.observe(this, Observer<Resource<CityResponse>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    val data = it.data
                    if(data!=null && data.data.isNotEmpty()){
                        selectedCity=data.data[0]
                        tvcityIntended.text=selectedCity.name
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> {
                }
            }
        })

        viewModel.sendRequestResult.observe(this,Observer {
            when(it?.status){
                Status.SUCCESS->{
                    textButtonConfirm.text="Konfirmasi"
                    confirmProgressBar.visibility=View.INVISIBLE
                    findNavController().navigateUp()
                }
                Status.ERROR->{
                    Toast.makeText(context,"Gagal menambah permintaan",Toast.LENGTH_SHORT).show()
                    textButtonConfirm.text="Konfirmasi"
                    confirmProgressBar.visibility=View.INVISIBLE
                }
                Status.LOADING->{
                    textButtonConfirm.text=""
                    confirmProgressBar.visibility=View.VISIBLE
                }
            }
        })
    }

    private fun onClick(view: View){
        view.bt_add_information.setOnClickListener {
            findNavController().navigate(R.id.listItemFragment)
        }

        view.tvcityIntended.setOnClickListener {
            val cityIntendedBottomSheet=CityIntendedBottomSheet(this)
            cityIntendedBottomSheet.show(activity!!.supportFragmentManager, cityIntendedBottomSheet.tag)
        }

        view.ic_arrow_right.setOnClickListener {
            val cityIntendedBottomSheet=CityIntendedBottomSheet(this)
            cityIntendedBottomSheet.show(activity!!.supportFragmentManager, cityIntendedBottomSheet.tag)
        }

        view.buttonConfirm.setOnClickListener {
            val addRequest = AddOrderRequest(
                selectedCity.upkid,
                selectedCity.sudinkotakabid,
                selectedCity.provinceid,
                editTextOrderNotes.text.toString(),
                adapterMedicine.getRequestedMedicine()
            )
            viewModel.sendRequestOrder(addRequest)
        }
    }

}
