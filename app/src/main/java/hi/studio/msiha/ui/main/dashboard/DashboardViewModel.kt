package hi.studio.msiha.ui.main.dashboard

import androidx.lifecycle.ViewModel
import hi.studio.msiha.data.entity.request.AddOrderRequest
import hi.studio.msiha.data.entity.response.AvailableMedicineResponse
import hi.studio.msiha.data.entity.response.CityResponse
import hi.studio.msiha.data.entity.response.OrderActiveResponse
import hi.studio.msiha.domain.interactor.GetOrder
import hi.studio.hipechat.util.LiveEvent
import hi.studio.hipechat.util.Resource

class DashboardViewModel(private val getOrder: GetOrder):ViewModel() {

    fun getAllActiveOrder(){
        getOrder.getAllOrderActive()
    }

    val activeOrderResult: LiveEvent<Resource<OrderActiveResponse>>
        get() = getOrder.allActiveOrderResult

    fun getAvailMed(){
        getOrder.getAvailMed()
    }

    fun getAllRequestOut(){
        getOrder.getAllRequestOut()
    }

    val allRequestResultOut: LiveEvent<Resource<OrderActiveResponse>>
        get() = getOrder.getAllRequestOut

    val availMedResult: LiveEvent<Resource<AvailableMedicineResponse>>
        get() = getOrder.availableMedicine

    fun getCities(){
        getOrder.getCities()
    }

    val sendRequestResult:LiveEvent<Resource<Void>>
        get() = getOrder.sendOrderRequestResult

    fun sendRequestOrder(addRequest: AddOrderRequest) {
        getOrder.sendOrderRequest(addRequest)
    }

    val allCities: LiveEvent<Resource<CityResponse>>
        get() = getOrder.allCitiesResult

}