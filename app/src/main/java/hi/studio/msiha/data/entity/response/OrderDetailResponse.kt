package hi.studio.msiha.data.entity.response

import hi.studio.msiha.data.entity.DistributionItems

data class OrderDetailResponse(
    val data: OrderDetail
)

data class OrderDetail(
    val id: String?,
    var orderCode: String? = null,
    val orderNumber: Int? = 0,
    val lastOrderStatus: String? = null,
    var lastApprovalStatus: String? = null,
    var orderNotes: String? = null,
    var approvalNotes: String? = null,
    var isRegular: Boolean? = true,
    var picID: Int? = 0,
    var logisticRole: String? = null,
    var senderName: String? = null,
    var upkIdOwner: Int? = 0,
    var sudinKabKotaIdOwner: Int? = 0,
    var provinceIdOwner: Int? = 0,
    var upkIdFor: Int? = 0,
    var sudinKabKotaFor: Int? = 0,
    var provinceIdFor: Int? = 0,
    var createdAt: String? = null,
    var updatedAt: String? = null,
    var createdByData: CreatedByData?,
    val orderItems: ArrayList<OrderItems>,
    val distributionPlans: ArrayList<DistributionItems>
)

data class OrderItems(
    val id: Int?,
    val packageUnitType: String?,
    val packageQuantity: String?,
    val notes: String?,
    val createdBy: String?,
    val createdAt: String?,
    val updatedAt: String?,
    val expiredDate: String?,
    val deletedAt: String?,
    val orderId: Int?,
    val medicineId: Int?,
    val medicine: MedicineItem
)

data class MedicineItem(
    val id: Int?,
    val name: String?,
    val codeName: String?,
    val medicineType: String?,
    val stockUnitType: String?,
    val packageUnitType: String?,
    val packageMultiplier: Int?,
    val ingredients: String?,
    val sediaan: String?,
    val imageUrl: String?,
    val createdBy: String?,
    val updatedBy: String?,
    val createdAt: String?,
    val updatedAt: String?,
    val deletedAt: String?
)

data class CreatedByData(
    val createdName: String?
)

