package hi.studio.msiha.ui.main.request

import androidx.lifecycle.ViewModel
import hi.studio.msiha.data.entity.response.OrderActiveResponse
import hi.studio.msiha.data.entity.response.OrderDetail
import hi.studio.msiha.domain.interactor.GetRequest
import hi.studio.hipechat.util.LiveEvent
import hi.studio.hipechat.util.Resource

class RequestViewModel(private val getRequest: GetRequest): ViewModel() {

    fun getAllRequest(){
        getRequest.getAllRequest()
    }

    fun getAllRequestOut(){
        getRequest.getAllRequestOut()
    }

    val allRequestResultOut: LiveEvent<Resource<OrderActiveResponse>>
        get() = getRequest.getAllRequestOut


    val allRequestResult: LiveEvent<Resource<OrderActiveResponse>>
        get() = getRequest.getAllRequest

    fun getRequestDetail(id: String){
        getRequest.getRequestDetail(id)
    }

    val getDetailResult: LiveEvent<Resource<OrderDetail>>
        get() = getRequest.getAllDetailRequest

}