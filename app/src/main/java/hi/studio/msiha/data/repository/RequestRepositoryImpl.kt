package hi.studio.msiha.data.repository

import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.entity.response.OrderActiveResponse
import hi.studio.msiha.data.source.local.contentprovider.TOKEN
import hi.studio.msiha.data.source.network.Endpoint
import hi.studio.msiha.domain.repository.RequestRepository
import kotlinx.coroutines.Deferred
import retrofit2.Response

class RequestRepositoryImpl(private val endpoint: Endpoint): RequestRepository {
    private var token: String = Hawk.get(TOKEN)

    override fun getAllActiveOrder(): Deferred<Response<OrderActiveResponse>> {
        return endpoint.getRequestAsync("Bearer "+Hawk.get(TOKEN))
    }
}