package hi.studio.msiha.data.repository

import android.util.Log
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.entity.request.AcceptanceDifferentRequest
import hi.studio.msiha.data.entity.request.SearchByQrCodeRequest
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.msiha.data.entity.response.OrdersInResponse
import hi.studio.msiha.data.source.local.contentprovider.TOKEN
import hi.studio.msiha.data.source.network.Endpoint
import hi.studio.msiha.domain.repository.AcceptanceRepository
import kotlinx.coroutines.Deferred
import retrofit2.Response

class AcceptanceRepositoryImpl(private val endpoint: Endpoint): AcceptanceRepository{

    private var token: String = Hawk.get(TOKEN)

    override fun getAllOrderLogisticInAsync(): Deferred<Response<OrdersInResponse>> {
        Log.d("tag31",  Hawk.get(TOKEN))
        return endpoint.getAllLogisticInAsync("Bearer " + Hawk.get(TOKEN))
    }

    override fun getOrderDetailLogisticInAsync(id:String): Deferred<Response<AcceptanceDetailResponse>> {
        return endpoint.getDetailLogisticInAsync("Bearer "+ Hawk.get(TOKEN), id)
    }

    override fun getQrCodeByIdAsync(id: SearchByQrCodeRequest): Deferred<Response<AcceptanceDetailResponse>> {
        return endpoint.getQrCodeByIdAsync("Bearer "+ Hawk.get(TOKEN), id)
    }

    override fun setStatusAcceptance(id: String?, status: String): Deferred<Response<Void>> {
        return endpoint.setStatusAcceptanceAsync("Bearer "+ Hawk.get(TOKEN), id, status)
    }

    override fun setStatusAcceptanceDiff(
        id: String?,
        data: AcceptanceDifferentRequest
    ): Deferred<Response<Void>> {
        return endpoint.setStatusAcceptanceDiffAsync("Bearer "+ Hawk.get(TOKEN), id, "ACCEPTED_DIFFERENT",data)
    }

    override fun getDetailAcceptance(id: String) {

    }

}