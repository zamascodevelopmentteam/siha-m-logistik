package hi.studio.msiha.data.entity

data class OrderActive(
    val id: String,
    var orderCode: String? = null,
    val orderNumber: Int? = 0,
    val lastOrderStatus : String? = null,
    var lastApprovalStatus : String? = null,
    var orderNotes : String? = null,
    var approvalNotes : String? = null,
    var isRegular  : Boolean? = true,
    var picID  : Int? = 0,
    var logisticRole  : String? = null,
    var upkIdOwner   : Int? = 0,
    var sudinKabKotaIdOwner  : Int? = 0,
    var provinceIdOwner  : Int? = 0,
    var upkIdFor   : Int? = 0,
    var sudinKabKotaFor   : Int? = 0,
    var provinceIdFor   : Int? = 0,
    var createdAt  : String? = null,
    var recieverName  : String? = null,
    var senderName  : String? = null,
    var updatedAt  : String? = null
    ) {
    companion object {
        fun empty() = OrderActive(
            "",
            "",
            0,
            "",
            "",
            "",
            "",
            true,
            0,
            "",
            0,
            0,
            0,
            0,
            0,
            0,
            "",
            "",
            "",
            ""
        )
    }
}