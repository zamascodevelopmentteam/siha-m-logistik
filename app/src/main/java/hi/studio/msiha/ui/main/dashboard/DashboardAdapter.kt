package hi.studio.msiha.ui.main.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.OrderActive
import hi.studio.msiha.data.source.local.contentprovider.*
import kotlinx.android.synthetic.main.list_active_request.view.*
import java.text.SimpleDateFormat
import java.util.*


class DashboardAdapter(
    private var orderActive: List<OrderActive>,
    private val context: Context
) : RecyclerView.Adapter<DashboardAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_active_request, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return orderActive.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val currentOrderActive = orderActive[position]
        if (currentOrderActive.lastOrderStatus != DRAFT) {
            if (currentOrderActive.lastOrderStatus != EVALUATION) {
                var stat: String = ""
                holder.invNum.text = currentOrderActive.orderCode
                stat = when {
                    currentOrderActive.lastApprovalStatus == "DRAFTING" -> {
                        holder.statusInv.setTextColor(context.resources.getColor(R.color.yellow))
                        "Draft"
                    }
                    currentOrderActive.lastApprovalStatus == "EXECUTED" -> "Selesai"
                    else -> {
                        holder.statusInv.setTextColor(context.resources.getColor(R.color.design_default_color_error))
                        "Terkonfirmasi"
                    }
                }

                var approvalStatus = currentOrderActive.lastOrderStatus
                when (approvalStatus) {
                    IN_EVALUATION -> approvalStatus = "Review"
                    EVALUATION -> approvalStatus = "Review"
                    CONFIRMED -> approvalStatus = "Sudah Disetujui"
                    PROCESSED -> approvalStatus = "Sedang Diproses"
                    EXECUTED -> approvalStatus = "Selesai"
                    DRAFT -> approvalStatus = "Draft"
                }

                val format2 = "yyyy-MM-dd"
                val formatWaktu = "hh:mm"
                val localeID = Locale("in", "ID")
                val simpleDateFormat2 = SimpleDateFormat(format2, localeID)
                val simpleDateFormatWaktu = SimpleDateFormat(formatWaktu, localeID)
                val date = simpleDateFormat2.parse(currentOrderActive.createdAt)

                holder.statusInv.text = approvalStatus
                holder.dateInv.text = simpleDateFormat2.format(date)
                holder.timeInv.text = simpleDateFormatWaktu.format(date) + " WIB"
            }
        }
    }

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val invNum = view.tv_no_inv_dashboard
        val statusInv = view.tv_status_inv_dashboard
        val dateInv = view.tv_date_inv_dashboard
        val timeInv = view.tv_time_inv_dashboard
    }
}