package hi.studio.msiha.domain.repository

import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.msiha.data.entity.response.DistributionResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response

interface DistributionRepository {
    fun getAllDistribution(): Deferred<Response<DistributionResponse>>
    fun getDetailRequest(id: String): Deferred<Response<AcceptanceDetailResponse>>


}