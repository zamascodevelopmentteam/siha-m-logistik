package hi.studio.hipechat.util.extension

import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.util.*

fun DateTime.toDisplayString(): String {
    return when {
        this.isToday() -> toString("HH:mm", Locale.US)
        this.isThisYear() -> toString("dd MMM", Locale.US)
        else -> toString("MM/dd/YYYY", Locale.US)
    }
}

fun DateTime.isToday() = toLocalDate().compareTo(LocalDate.now()) == 0

fun DateTime.isThisYear() = year == DateTime().year