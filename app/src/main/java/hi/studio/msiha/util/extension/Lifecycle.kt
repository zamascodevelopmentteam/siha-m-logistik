/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.hipechat.util.extension

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import hi.studio.msiha.domain.model.Failure


/**
 * Created by mochadwi on 2019-10-27
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) =
    liveData.observe(this, Observer(body))

fun <L : LiveData<Failure>> LifecycleOwner.failure(liveData: L, body: (Failure?) -> Unit) =
    liveData.observe(this, Observer(body))