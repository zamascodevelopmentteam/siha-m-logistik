package hi.studio.msiha.domain.repository

import hi.studio.msiha.data.entity.response.LoginResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response

interface AuthRepository {
    fun login(nik:String,password:String): Deferred<Response<LoginResponse>>
}