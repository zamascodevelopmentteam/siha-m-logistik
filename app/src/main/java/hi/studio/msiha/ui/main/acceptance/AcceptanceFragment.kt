package hi.studio.msiha.ui.main.acceptance


import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.OrderLogisticIn
import hi.studio.msiha.data.entity.response.User
import hi.studio.msiha.data.source.local.contentprovider.USER
import hi.studio.msiha.ui.main.MainActivity
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.fragment_acceptance.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class AcceptanceFragment : Fragment(), OnOrderClick, SearchView.OnQueryTextListener {

    private val viewModel: AcceptanceViewModel by viewModel()
    private var adapterAcceptance: AcceptanceAdapter = AcceptanceAdapter(ArrayList(), this)
    private lateinit var menuItem: Menu
    private lateinit var menuItemSearch: MenuItem
    private lateinit var menuItemScan: MenuItem
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.setHasOptionsMenu(true)
        if((Hawk.get(USER) as User).logisticrole.toString() == "MINISTRY_ENTITY"){
            (activity as MainActivity).supportActionBar?.title = "Pembelian"
        }else{
            (activity as MainActivity).supportActionBar?.title = "Penerimaan"
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_acceptance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFabButton()
        viewModel.getOrdersIn()
        observeLiveData()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_search, menu)
        menuItemSearch = menu.findItem(R.id.menuSearch)
        menuItemScan = menu.findItem(R.id.menuScan)
        val searchView: SearchView = menuItemSearch.actionView as SearchView

        menuItemScan.setVisible(true)
//        menuItemSearch.setVisible(false)
        searchView.setOnQueryTextListener(this)
        this.menuItem = menu
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menuScan -> findNavController().navigate(R.id.scanqrFragment)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleOrderInAdapter(orderList2: List<OrderLogisticIn>) {
        adapterAcceptance = AcceptanceAdapter(orderList2, this)

        recyclerViewAcceptance.apply {
            adapter = adapterAcceptance
            layoutManager = LinearLayoutManager(activity)
        }
    }

    private fun observeLiveData() {
        viewModel.getOrdersInResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (!it.data.isNullOrEmpty()) {
                        progressBarAcceptance.visibility = View.INVISIBLE
                        if (it.data.isNotEmpty()) {
//                            menuItemScan.setVisible(true)
//                            menuItemSearch.setVisible(true)
                            handleOrderInAdapter(it.data)
                            textEmpty.visibility = View.INVISIBLE
                            recyclerViewAcceptance.visibility = View.VISIBLE
                        } else {
                            progressBarAcceptance.visibility = View.INVISIBLE
                            textEmpty.visibility = View.VISIBLE
                            recyclerViewAcceptance.visibility = View.INVISIBLE
                        }
                    } else {
//                        menuItemScan.setVisible(false)
//                        menuItemSearch.setVisible(false)
                        progressBarAcceptance.visibility = View.INVISIBLE
                        textEmpty.visibility = View.VISIBLE
                        recyclerViewAcceptance.visibility = View.INVISIBLE
                    }
                }
                Status.LOADING -> {
                    progressBarAcceptance.visibility = View.VISIBLE
                }

                Status.ERROR -> {
//                    menuItemScan.setVisible(false)
//                    menuItemSearch.setVisible(false)
                    progressBarAcceptance.visibility = View.GONE
                    textEmpty.visibility = View.VISIBLE
                    recyclerViewAcceptance.visibility = View.INVISIBLE
                }

            }
        })
    }


    private fun setupFabButton() {
        fab_add_acceptance.setOnClickListener {
            //            findNavController().navigate(R.id.detailAcceptanceFragment)
        }
    }

    override fun onClick(orderIn: OrderLogisticIn) {
        val bundle = Bundle()
        bundle.putString("id", orderIn.id)
        findNavController().navigate(R.id.detailAcceptanceFragment, bundle)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        adapterAcceptance.filter.filter(query.toString())
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        adapterAcceptance.filter.filter(newText.toString())
        return false
    }



}
