package hi.studio.msiha.ui.main.acceptance

import androidx.lifecycle.ViewModel
import hi.studio.msiha.data.entity.OrderLogisticIn
import hi.studio.msiha.data.entity.request.AcceptanceDifferentRequest
import hi.studio.msiha.data.entity.request.SearchByQrCodeRequest
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.msiha.domain.interactor.GetAcceptance
import hi.studio.hipechat.util.LiveEvent
import hi.studio.hipechat.util.Resource

class AcceptanceViewModel(private val getAcceptance: GetAcceptance):ViewModel(){

    val getOrdersInResult: LiveEvent<Resource<List<OrderLogisticIn>>>
        get() = getAcceptance.getAllOrderInResult

    fun getOrdersIn(){
        getAcceptance.getAllOrdersIn()
    }

    val getOrdersDetailInResult: LiveEvent<Resource<AcceptanceDetailResponse>>
        get() = getAcceptance.getAllOrderDetailInResult

    fun getOrdersDetailIn(id: String){
        getAcceptance.getAllOrdersDetailIn(id)
    }

    val getQrCodeResult: LiveEvent<Resource<AcceptanceDetailResponse>>
        get() = getAcceptance.getQrCodeByIdResult

    fun getQrCodeById(id: SearchByQrCodeRequest){
        getAcceptance.getQrCodeById(id)
    }

    val setStatusAcceptanceResult: LiveEvent<Resource<Void>>
        get() = getAcceptance.setAcceptanceResult

    fun setStatusAcceptance(id: String?, Status: String){
        getAcceptance.setStatusAcceptance(id, Status)
    }

    val setStatusAcceptanceDiffResult: LiveEvent<Resource<Void>>
        get() = getAcceptance.setAcceptanceDiffResult

    fun setStatusAcceptanceDiff(id: String?, data: AcceptanceDifferentRequest){
        getAcceptance.setStatusAcceptanceDiff(id, data)
    }
}