package hi.studio.msiha.data.entity

data class DistributionPlanItems(
    var distPlanItemId: Int? = 0,
    var packageUnitType: String? = null,
    val expiredDate: String? = null,
    val notes: String? = null,
    var packageQuantity: Int? = 0,
    val medicine: Medicine? = null,
    val inventory: Inventory? = null
)

data class Medicine(
    val name: String? = null,
    val codeName: String? = null
)

data class Inventory(
    val batchCode: String? = null
)