package hi.studio.msiha.ui.main.request.detailorder


import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.DistributionItems
import hi.studio.msiha.data.entity.response.OrderDetail
import hi.studio.msiha.data.source.local.contentprovider.*
import hi.studio.msiha.ui.main.distribution.OnDetailClick
import hi.studio.msiha.ui.main.request.RequestViewModel
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.fragment_detail_order.*
import kotlinx.android.synthetic.main.fragment_detail_order.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class DetailRequestFragment : Fragment(), OnDetailClick {

    private val viewModel: RequestViewModel by viewModel()
    private lateinit var itemsAdapter: DetailRequestAdapter
    private lateinit var itemsDistAdapter: DistributionPlanAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProgressBar()
        val id = arguments?.getString("id").toString()
        viewModel.getRequestDetail(id)
        observeLiveData()
        bt_confirm.visibility = View.INVISIBLE

        view.distribution_plan_add.setOnClickListener {
            findNavController().navigate(R.id.distributionPlanFragment)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
//        inflater.inflate(R.menu.menu_edit, menu)
    }

    private fun showProgressBar() {
        progressBarDetailRequest.visibility = View.VISIBLE
        wrapperDetailOrder.visibility = View.GONE
    }

    private fun hideProgressBar() {
        progressBarDetailRequest.visibility = View.GONE
        wrapperDetailOrder.visibility = View.VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuEdit -> findNavController().navigate(R.id.editOrderFragment)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun bindItem(item: OrderDetail) {
        val format2 = "yyyy-MM-dd"
        val localeID = Locale("in", "ID")
        val simpleDateFormat2 = SimpleDateFormat(format2, localeID)
        val make = simpleDateFormat2.parse(item.createdAt)
        val edit = simpleDateFormat2.parse(item.updatedAt)

        var approvalStatus = item.lastApprovalStatus
        when (approvalStatus) {
            IN_EVALUATION -> approvalStatus = "Review"
            CONFIRMED -> approvalStatus = "Sudah Disetujui"
            APPROVED_FULLY -> approvalStatus = "Buat Usulan Kebutuhan"
            APPROVED_FULL -> approvalStatus = "Buat Usulan Kebutuhan"
            REJECTED_INTERNAL -> approvalStatus = "Tidak Disetujui"
            REJECTED_UPPER -> approvalStatus = "Tidak Membuat Usulan Kebutuhan"
            DRAFT -> approvalStatus = "Draft"
        }

        var accStatus = item.lastOrderStatus
        when (accStatus) {
            EXECUTED -> accStatus = "Diterima"
            DRAFT -> accStatus = "Draft"
            PROCESSED -> accStatus = "Dalam Proses"
            EVALUATION -> accStatus = "Dalam Evaluasi"
            ACCEPTED_FULLY -> accStatus = "Diterima Penuh"
            ACCEPTED_DIFFERENT -> accStatus = "Diterima Sebagian"
            LOST -> accStatus = "Hilang"
            REJECTED -> accStatus = "Ditolah"
        }

        itemsAdapter = DetailRequestAdapter(item.orderItems)
        itemsDistAdapter = DistributionPlanAdapter(item.distributionPlans, this@DetailRequestFragment)

        rv_order_inf.apply {
            adapter = itemsAdapter
            layoutManager = LinearLayoutManager(activity)
        }

        rv_order_dist.apply {
            adapter = itemsDistAdapter
            layoutManager = LinearLayoutManager(activity)
        }


        tv_inv_detail.text = item.orderCode
        tv_status_app_detail.text = approvalStatus
        tv_status_acc_detail.text = accStatus
        tv_intended_detail.text = item.senderName
        tv_note_detail.text = item.orderNotes
        tv_note_app_detail.text = item.approvalNotes
        tv_date_make_detail.text = simpleDateFormat2.format(make)
        tv_date_edit_detail.text = simpleDateFormat2.format(edit)
        tv_make_by_detail.text = item.createdByData?.createdName
//        tv_edit_by_detail.text = item.createdByData?.createdName
    }

    private fun observeLiveData() {
        viewModel.getDetailResult.observe(this, Observer<Resource<OrderDetail>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        hideProgressBar()
                        bindItem(it.data)
                        if(it.data.distributionPlans.isNotEmpty()){
                            textView12.visibility = View.INVISIBLE
                        }
                    } else {
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> {
                }
            }
        })
    }

    override fun onClick(orderIn: DistributionItems) {
        val bundle = Bundle()
        bundle.putString("id", orderIn.id.toString())
        findNavController().navigate(R.id.detailDistributionFragment, bundle)
    }

}
