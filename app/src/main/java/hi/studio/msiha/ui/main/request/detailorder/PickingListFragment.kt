package hi.studio.msiha.ui.main.request.detailorder


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController


import hi.studio.msiha.R
import kotlinx.android.synthetic.main.fragment_picking_list.view.*

/**
 * A simple [Fragment] subclass.
 */
class PickingListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_picking_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.bt_confirm_pick_list.setOnClickListener {
            findNavController().navigateUp()
        }
    }
}
