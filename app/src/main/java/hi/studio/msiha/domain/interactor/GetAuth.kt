package hi.studio.msiha.domain.interactor

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.entity.response.LoginResponse
import hi.studio.msiha.data.source.local.contentprovider.TOKEN
import hi.studio.msiha.data.source.local.contentprovider.USER
import hi.studio.msiha.domain.repository.AuthRepository
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.toSingleEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class GetAuth (private val authRepository: AuthRepository, val context: Context): CoroutineScope {
    val loginResult = MutableLiveData<Resource<LoginResponse>>().toSingleEvent()

    override val coroutineContext: CoroutineContext = Dispatchers.IO

    fun login(nik: String, password: String) {
        val res = authRepository.login(nik, password)
        loginResult.postValue(Resource.loading(null))
        launch {
            val data = res.await()
            if (!data.isSuccessful) {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    loginResult.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    loginResult.postValue(Resource.error("Internal Server Error", null))
                }
            } else {
                val rsp = data.body()
                if (rsp != null) {
                    //save token
                    Log.d("TAG3", rsp.data.toString())
                    Hawk.put(USER, rsp.data.user)
                    Hawk.put(TOKEN, rsp.data.token)
                }else{
                    Timber.d("rsp null")
                }
                try {

                } catch (e: Error) {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    loginResult.postValue(Resource.error(msg,null))
                    return@launch
                }
                loginResult.postValue(Resource.success(rsp))
            }
        }
    }

}
