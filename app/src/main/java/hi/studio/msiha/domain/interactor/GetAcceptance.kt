package hi.studio.msiha.domain.interactor

import android.content.Context
import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.data.entity.OrderLogisticIn
import hi.studio.msiha.data.entity.request.AcceptanceDifferentRequest
import hi.studio.msiha.data.entity.request.SearchByQrCodeRequest
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.msiha.domain.repository.AcceptanceRepository
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.toSingleEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class GetAcceptance(
    private val acceptanceRepository: AcceptanceRepository,
    val context: Context
) : CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.IO

    val getAllOrderInResult = MutableLiveData<Resource<List<OrderLogisticIn>>>().toSingleEvent()
    val getAllOrderDetailInResult =
        MutableLiveData<Resource<AcceptanceDetailResponse>>().toSingleEvent()
    val setAcceptanceResult = MutableLiveData<Resource<Void>>().toSingleEvent()
    val setAcceptanceDiffResult = MutableLiveData<Resource<Void>>().toSingleEvent()
    val getQrCodeByIdResult = MutableLiveData<Resource<AcceptanceDetailResponse>>().toSingleEvent()

    fun getAllOrdersIn() {
        val response = acceptanceRepository.getAllOrderLogisticInAsync()
        getAllOrderInResult.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.isSuccessful) {
                val orders = data.body()
                getAllOrderInResult.postValue(Resource.success(orders?.data))
            } else {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    getAllOrderInResult.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    getAllOrderInResult.postValue(Resource.error("Internal Server Error", null))
                }
            }
        }
    }

    fun getAllOrdersDetailIn(id: String) {
        val response = acceptanceRepository.getOrderDetailLogisticInAsync(id)
        getAllOrderDetailInResult.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.isSuccessful) {
                val orders = data.body()
                getAllOrderDetailInResult.postValue(Resource.success(orders))
            } else {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    getAllOrderDetailInResult.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    getAllOrderDetailInResult.postValue(
                        Resource.error(
                            "Internal Server Error",
                            null
                        )
                    )
                }
            }
        }
    }

    fun getQrCodeById(id: SearchByQrCodeRequest) {
        val response = acceptanceRepository.getQrCodeByIdAsync(id)
        getQrCodeByIdResult.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.isSuccessful) {
                val orders = data.body()
                getQrCodeByIdResult.postValue(Resource.success(orders))
            } else {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    getQrCodeByIdResult.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    getQrCodeByIdResult.postValue(Resource.error("${id.droppingNumber} Tidak Ditemukan", null))
                }
            }
        }
    }

    fun setStatusAcceptance(id: String?, status: String) {
        val response = acceptanceRepository.setStatusAcceptance(id, status)
        setAcceptanceResult.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.isSuccessful) {
                val orders = data.body()
                setAcceptanceResult.postValue(Resource.success(orders))
            } else {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    setAcceptanceResult.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    setAcceptanceResult.postValue(Resource.error("Internal Server Error", null))
                }
            }
        }
    }

    fun setStatusAcceptanceDiff(id: String?, data: AcceptanceDifferentRequest) {
        val response = acceptanceRepository.setStatusAcceptanceDiff(id, data)
        setAcceptanceDiffResult.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (data.isSuccessful) {
                val orders = data.body()
                setAcceptanceDiffResult.postValue(Resource.success(orders))
            } else {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    setAcceptanceDiffResult.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    setAcceptanceDiffResult.postValue(Resource.error("Internal Server Error", null))
                }
            }
        }
    }
}