/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.hipechat.data.source.network.refreshauthenticator.delegate


/**
 * Created by mochadwi on 2019-11-03
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

interface OauthRefreshAuthenticatorDelegate {

    // Used to retrieve the user's oauth bearer authToken:
    fun userOauthBearerToken(): String

    // Used to retrieve the user's oauth refresh authToken:
    fun userOauthRefreshToken(): String

    // Used to request a user's refreshed oauth bearer authToken:
    fun userRequestNewBearerToken(): String?

    // Used to signal that the oauth session is not recoverable
    fun userOauthSessionNotRecoverable()
}