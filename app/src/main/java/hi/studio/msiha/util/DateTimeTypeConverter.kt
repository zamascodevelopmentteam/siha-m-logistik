package hi.studio.hipechat.util

import com.google.gson.*
import org.joda.time.DateTime
import java.lang.reflect.Type
import java.util.*


class DateTimeTypeConverter : JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

    override fun serialize(src: DateTime?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return JsonPrimitive(src?.toString())
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): DateTime? {
        return try{
            val dateStr = json?.asString
            if (dateStr == null || dateStr == "") {
                return null
            }
            DateTime(dateStr)
        } catch (e : IllegalArgumentException){
            val date = context!!.deserialize<Date>(json, Date::class.java)
            DateTime(date)
        }
    }

}