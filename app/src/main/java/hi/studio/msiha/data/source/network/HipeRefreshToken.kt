/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.msiha.data.source.network

import android.content.Context
import okhttp3.*

class HipeRefreshToken(val context: Context, val ENDPOINT: String) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return null
    }
}
//    companion object {
//        val MEDIA_TYPE_JSON: MediaType = MediaType.parse("application/json; charset=utf-8")!!
//    }
//
//    @Throws(IOException::class)
//    private fun refreshToken(): String {
//        val client = OkHttpClient()
//
//        var token = PreferencesHelper.getStringPreference(context.contentResolver, PREF_USER_TOKEN_AUTH)
//        var refreshToken = PreferencesHelper.getStringPreference(context.contentResolver, PREF_USER_REFRESH_TOKEN_AUTH)
//        var ttlToken = PreferencesHelper.getStringPreference(context.contentResolver, PREF_USER_TTL_TOKEN_AUTH)
//
//        if (token == "") {
//            return ""
//        }
//
//        if (!TextUtils.isEmpty(token)) {
//            token = token.substring(7)
//        }
//        val tokenReq = refreshToken
//        val gson = Gson()
//        val req = Request.Builder()
//            .url("${ENDPOINT}auth/refresh")
//            .post(RequestBody.create(MEDIA_TYPE_JSON, tokenReq))
//            .build()
//        val res = client.newCall(req).execute()
//        if (!res.isSuccessful) {
//            res.close()
//            throw IOException("Unexpected code $res")
//        }
//        val keyToken = Token.fromJSON(gson, res.body()!!.string())
//
//        res.body()!!.close()
//        res.close()
//        return PreferencesHelper.getStringPreference(context.contentResolver, PREF_USER_TOKEN_AUTH)
//    }
//
//    @Throws(IOException::class)
//    override fun authenticate(route: Route?, response: Response): Request? {
//        var token =
//
//        // ignore auth login endpoint
//        if(response.request().url().toString().endsWith("/auth/login")){
//            return null
//        }
//
//        // handle refresh token if token is not empty and status code = 401
//        if (token.access != "" && response.code() == 401) {
//            token.access = refreshToken()
//            // if refresh token failed or retried 3 times
//            if (token.access == "" || responseCount(response)>=3) {
//                return null
//            }
//
//            return response.request().newBuilder()
//                .header("Authorization", "Bearer ${token.access}").build()
//
//        }
//        return response.request()
//    }
//
//    private fun responseCount(response: Response?): Int {
//        var result = 1
//        while (response?.priorResponse() != null) {
//            result++
//        }
//        return result
//    }
