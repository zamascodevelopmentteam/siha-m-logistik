package hi.studio.msiha.data.source.network


import hi.studio.msiha.data.entity.request.AcceptanceDifferentRequest
import hi.studio.msiha.data.entity.request.AddOrderRequest
import hi.studio.msiha.data.entity.request.SearchByQrCodeRequest
import hi.studio.msiha.data.entity.request.UserLoginRequest
import hi.studio.msiha.data.entity.response.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface Endpoint {

    @POST("public/auth/login")
    fun loginAsync(@Body userLogin: UserLoginRequest): Deferred<Response<LoginResponse>>

    @GET("private/orders-logistic/in")
    fun getAllActiveOrderAsync(
        @Header("Authorization") token: String
    ): Deferred<Response<OrderActiveResponse>>

    @GET("private/orders-logistic")
    fun getRequestAsync(
        @Header("Authorization") token: String
    ): Deferred<Response<OrderActiveResponse>>

    @GET("private/inventory?upkId=1191&sudinKabKotumId&provinceId")
    fun getAvailMedAsync(
        @Header("Authorization") token: String
    ): Deferred<Response<AvailableMedicineResponse>>

    @GET("private/orders-logistic/availableFor")
    fun getAllCityAsync(
        @Header("Authorization") token: String
    ): Deferred<Response<CityResponse>>

    @GET("/private/penerimaan")
    fun getAllLogisticInAsync(
        @Header("Authorization") token: String
    ): Deferred<Response<OrdersInResponse>>

    @GET("private/penerimaan/{id}")
    fun getDetailLogisticInAsync(
        @Header("Authorization") token: String,
        @Path("id") id: String
    ): Deferred<Response<AcceptanceDetailResponse>>

    @POST("private/penerimaan-bycode")
    fun getQrCodeByIdAsync(
        @Header("Authorization") token: String,
        @Body id: SearchByQrCodeRequest
    ): Deferred<Response<AcceptanceDetailResponse>>

    @POST("private/orders-logistic/create")
    fun sendOrderRequestAsync(
        @Header("Authorization") token: String,
        @Body addOrderRequest: AddOrderRequest
    ): Deferred<Response<Void>>

    @GET("private/orders/{orderId}")
    fun getDetailRequest(
        @Path("orderId") id: String,
        @Header("Authorization") token: String
    ): Deferred<Response<OrderDetailResponse>>

    @GET("private/pengiriman")
    fun getAllDistributionAsycn(
        @Header("Authorization") token: String
    ): Deferred<Response<DistributionResponse>>

    @POST("private/penerimaan/accept/{id}")
    fun setStatusAcceptanceAsync(
        @Header("Authorization") token: String,
        @Path("id") id: String?,
        @Query("status") status: String
    ): Deferred<Response<Void>>

    @POST("private/penerimaan/accept/{id}")
    fun setStatusAcceptanceDiffAsync(
        @Header("Authorization") token: String,
        @Path("id") id: String?,
        @Query("status") status: String,
        @Body data: AcceptanceDifferentRequest
    ): Deferred<Response<Void>>
}
