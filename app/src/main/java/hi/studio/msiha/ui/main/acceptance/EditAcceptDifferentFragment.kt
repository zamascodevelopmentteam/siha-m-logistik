package hi.studio.msiha.ui.main.acceptance


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.DetailAcceptance
import hi.studio.msiha.data.entity.request.AcceptanceDifferentRequest
import hi.studio.msiha.data.entity.request.DistributionPlans
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.fragment_edit_accept_different.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class EditAcceptDifferentFragment : Fragment(){

    private lateinit var editAcceptanceAdapter: EditAcceptDifferentAdapter
    private val viewModel: AcceptanceViewModel by viewModel()
    lateinit var sting: AcceptanceDifferentRequest
    lateinit var id: String
    private var listData: ArrayList<DistributionPlans> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_accept_different, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProgressBar()
        id = arguments?.getString("id").toString()
        viewModel.getOrdersDetailIn(id)
        observeLiveData()
        sendData()
    }

    override fun onResume() {
        super.onResume()
        observeLiveData()
    }

    private fun bindItem(detailAcceptance: DetailAcceptance) {
        editAcceptanceAdapter = EditAcceptDifferentAdapter(
            detailAcceptance.distributionPlanItems
        )
        rvEditDifferent.apply {
            adapter = editAcceptanceAdapter
            layoutManager = LinearLayoutManager(activity)
            setItemViewCacheSize(detailAcceptance.distributionPlanItems.size)
        }

    }

    private fun sendData() {
        buttonEditAccDone.setOnClickListener {
            val distributionItems = editAcceptanceAdapter.getItem()
            for (item in distributionItems) {
                listData.add(
                    DistributionPlans(
                        id = item.distPlanItemId?.toInt(),
                        actualReceivedPackageQuantity = item.packageQuantity?.toInt()
                    )
                )
            }
            sting = AcceptanceDifferentRequest(listData)
            viewModel.setStatusAcceptanceDiff(id, sting)
            //Toast.makeText(activity, sting.toString(),Toast.LENGTH_SHORT).show()
        }
    }


    private fun showProgressBar() {
        progressBarEditAccept.visibility = View.VISIBLE
        layoutWrapper.visibility = View.GONE
    }

    private fun hideProgressBar() {
        progressBarEditAccept.visibility = View.GONE
        layoutWrapper.visibility = View.VISIBLE
    }

    private fun observeLiveData() {
        viewModel.getOrdersDetailInResult.observe(
            viewLifecycleOwner,
            Observer<Resource<AcceptanceDetailResponse>?> {
                when (it?.status) {
                    Status.SUCCESS -> {
                        if (it.data?.data != null) {
                            bindItem(it.data.data)
                            hideProgressBar()
                        } else {
                            hideProgressBar()
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {

                    }
                }
            })

        viewModel.setStatusAcceptanceDiffResult.observe(viewLifecycleOwner, Observer<Resource<Void>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    findNavController().navigateUp()
//                    val confirmBottomSheet = BottomSheetSuccess()
//                    buttonConfirmReceive.setOnClickListener {
//                        confirmBottomSheet.show(
//                            activity!!.supportFragmentManager,
//                            confirmBottomSheet.tag
//                        )
//                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> {

                }
            }
        })

    }

}
