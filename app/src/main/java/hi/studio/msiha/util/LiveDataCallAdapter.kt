package hi.studio.hipechat.util


import androidx.lifecycle.LiveData
import com.google.gson.JsonSyntaxException
import hi.studio.msiha.data.source.network.ApiResponse
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.lang.reflect.Type
import java.util.concurrent.atomic.AtomicBoolean

/**
 * A Retrofit adapter that converts the Call into a LiveData of ApiResponse.
 * @param <R>
</R> */
class LiveDataCallAdapter<R>(private val responseType: Type) :
    CallAdapter<R, LiveData<ApiResponse<R>>> {

    override fun responseType() = responseType

    override fun adapt(call: Call<R>): LiveData<ApiResponse<R>> {
        return object : LiveData<ApiResponse<R>>() {
            private var started = AtomicBoolean(false)
            override fun onActive() {
                super.onActive()
                if (started.compareAndSet(false, true)) {
                    call.enqueue(object : Callback<R> {
                        override fun onResponse(call: Call<R>, response: Response<R>) {
                            Timber.d(">>> onResponse = ${response}")
                            postValue(ApiResponse.create(response))
                        }

                        override fun onFailure(call: Call<R>, throwable: Throwable) {
                            Timber.d(">>> onFailure = ${throwable}")
                            throwable.printStackTrace()
                            when(throwable) {
                                is JsonSyntaxException -> {
                                    postValue(ApiResponse.Companion.create(Error("Parsing JSON failed")))
                                }
                                else -> postValue(ApiResponse.create(throwable))
                            }
                        }
                    })
                }
            }
        }
    }
}
