package hi.studio.msiha.data.entity


data class DistributionItems(
    val approvalNotes: String?,
    val approvalStatus: String?,
    val createdAt: String?,
    val createdBy: String?,
    val deletedAt: String?,
    val droppingNumber: String?,
    val fundSource: String?,
    val id: Int?,
    val logisticRole: String?,
    val notes: String?,
    val orderId: Int?,
    val picId: String?,
    val planStatus: String?,
    val provinceId: String?,
    val recieverName: String?,
    val senderName: String?,
    val sudinKabKotaId: String?,
    val updatedAt: String?,
    val updatedBy: String?,
    val upkId: Int?
)