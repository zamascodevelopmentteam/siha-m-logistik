package hi.studio.msiha.ui.main.request

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.OrderActive
import hi.studio.msiha.data.entity.response.OrderActiveResponse
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.fragment_request_in.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RequestInFragment : Fragment(), OnItemClick, SearchView.OnQueryTextListener {

    private var orderAdapter: RequestAdapter = RequestAdapter(ArrayList(), 0,this@RequestInFragment)
    private val viewModel: RequestViewModel by viewModel()
    private lateinit var menuItem: Menu
    private lateinit var menuItemSearch: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_request_in, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_search, menu)
        menuItemSearch = menu.findItem(R.id.menuSearch)

        val searchView: SearchView = menuItemSearch.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        this.menuItem = menu
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()
        viewModel.getAllRequest()
    }

    override fun onResume() {
        super.onResume()
        observeLiveData()
    }

    private fun handleListOrder(orderList: ArrayList<OrderActive>) {
        orderAdapter = RequestAdapter(orderList, 1,this@RequestInFragment)

        rv_orderIn.apply {
            adapter = orderAdapter
            layoutManager = LinearLayoutManager(activity) as RecyclerView.LayoutManager?
        }

    }

    private fun observeLiveData(){
        viewModel.allRequestResult.observe(this, Observer<Resource<OrderActiveResponse>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        progressBarRequestIn.visibility = View.INVISIBLE
                        handleListOrder(it.data.data)
                    } else {
                        textEmptyReqIn.visibility = View.VISIBLE
                    }
                }
                Status.ERROR -> {
                    textEmptyReqIn.visibility = View.VISIBLE
                    progressBarRequestIn.visibility = View.GONE
                }
                Status.LOADING -> {
                }
            }
        })
    }

    override fun onItemClicked(order: OrderActive) {
        val bundle = Bundle()
        bundle.putString("id", order.id)
        findNavController().navigate(R.id.detailOrderFragment, bundle)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        orderAdapter.filter.filter(query.toString())
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        orderAdapter.filter.filter(newText.toString())
        return false
    }

}
