package hi.studio.msiha.ui.main.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.AvailableMedicine
import hi.studio.msiha.util.view.DatePickerBottomSheet
import hi.studio.msiha.util.view.DatePickerDialogBot
import hi.studio.hipechat.util.extension.textToDate
import kotlinx.android.synthetic.main.bottom_sheet_add_medicine.*

interface ConfirmAddMed{
    fun addMed(medicine: AvailableMedicine)
}

class BottomSheetAddMedicine(
    private var medicine:AvailableMedicine,
    private val confirmAddMed: ConfirmAddMed
    ): BottomSheetDialogFragment(), DatePickerDialogBot {

    //private val viewModel:AddMedicineViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.bottom_sheet_add_medicine, container, false)
        dialog!!.setCanceledOnTouchOutside(false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateView()
        setupDatePicker()
        setupCloseButton()
        setupButtonConfirm()
    }

    override fun datePicker(dateToServer: String, dateToTextView: String) {
        textExpiredDate.text=dateToTextView
        medicine.expiredDate=dateToServer
    }

    private fun populateView(){
        textMedUnit.text=medicine.packageUnit
        textViewMedName.text=medicine.medicineName
        if(medicine.requestedQuantity!=null){
            editTextJumlahBarang.setText("{$medicine.requestedQuantity}")
            textExpiredDate.text = medicine.expiredDate?.textToDate()
        }
    }

    private fun setupDatePicker(){
        val defaultDate = if(medicine.expiredDate.isNullOrEmpty()){
            ""
        }else{
            medicine.expiredDate
        }
        buttonDatePicker.setOnClickListener {
            val datePicker = DatePickerBottomSheet(defaultDate.toString(),this)
            datePicker.show(childFragmentManager, "datepicker")
        }
    }

    private fun setupCloseButton(){
        buttonCloseBottomSheet.setOnClickListener {
            this.dismiss()
        }
    }

    private fun setupButtonConfirm(){
        buttonConfirm.setOnClickListener {
            if(isValid()){
                medicine.requestedQuantity=editTextJumlahBarang.text.toString().toInt()
                confirmAddMed.addMed(medicine)
                this.dismiss()
            }else{
                Toast.makeText(context,"Jumlah Barang dan Expired Date harus diisi",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun isValid():Boolean{
        return if(this.tag=="edit_obat"){
            (textExpiredDate.text.isNotEmpty())
        }else{
            (textExpiredDate.text.isNotEmpty() && editTextJumlahBarang.text.toString()!="0")
        }
    }


}