package hi.studio.msiha.util.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import androidx.core.app.NotificationCompat
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.os.Build
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.data.source.local.contentprovider.PREF_DEVICE_TOKEN
import org.koin.android.ext.android.inject
import timber.log.Timber

class SihaFirebaseMessagingService : FirebaseMessagingService() {

    val context: Context by inject()
    var localBroadcastManager = LocalBroadcastManager.getInstance(context)
    var nid=1
    private var channelID = "hipe_channel"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // ...
        val mNotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val fromSender =remoteMessage.data["sender"].toString()
        val type =remoteMessage.data["type"].toString()
        val content =remoteMessage.data["content"].toString()

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("tag12", "From: ${remoteMessage.from}")

        val builder = generatePersonalMessageBuilder(remoteMessage)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(channelID)
            builder.setVibrate(longArrayOf(1000, 1000))

            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()
//            val sound = Uri.parse("android.resource://"
//                    + applicationContext.packageName + "/" + R.raw.n)
            val mChannel = NotificationChannel(channelID,type,NotificationManager.IMPORTANCE_HIGH)
            mChannel.description=content
//            mChannel.setSound(sound,attributes)
            mChannel.enableVibration(true)
            mChannel.enableLights(true)
            mChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            mNotificationManager.createNotificationChannel(mChannel)
        }

        val notification = NotificationCompat.InboxStyle(builder)
        mNotificationManager.notify(nid, notification.build())

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d("tag12", "Message Notification Body: ${it.body}")
        }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    fun sendBroadcastMessage() {
        Timber.d("send broadcast")
        val localIntent = Intent("message_notification")
            .putExtra("data", "refresh")
        localBroadcastManager.sendBroadcast(localIntent)
    }

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        Hawk.put(PREF_DEVICE_TOKEN, token)
        Log.d("tag12", "Message data payload: " )
//        sendRegistrationToServer(token)
    }

    private fun generatePersonalMessageBuilder(remoteMessage: RemoteMessage): NotificationCompat.Builder {
        val content = remoteMessage.data["content"].toString()
        val content2 = remoteMessage.notification?.body
        val subject = remoteMessage.data["subject"].toString()
        val fromSender = remoteMessage.data["sender"].toString()
        val topicId = remoteMessage.data["topic_id"].toString()
        val type = remoteMessage.data["type"].toString()

        return androidx.core.app.NotificationCompat.Builder(this, type)
            .setSmallIcon(R.mipmap.icon_logistik)
            .setContentTitle("Siha Logistik")
            .setContentText(content2)
            .setAutoCancel(true)
            .setDefaults(androidx.core.app.NotificationCompat.DEFAULT_VIBRATE)
            .setStyle(androidx.core.app.NotificationCompat.BigTextStyle().bigText(content))
            .setPriority(androidx.core.app.NotificationCompat.PRIORITY_MAX)
//            .setContentIntent(generatePendingIntent(remoteMessage))
    }


}