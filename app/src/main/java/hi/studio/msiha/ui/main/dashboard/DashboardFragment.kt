package hi.studio.msiha.ui.main.dashboard


import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.view.menu.MenuBuilder
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.hipechat.util.Resource
import com.orhanobut.hawk.Hawk

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.OrderActive
import hi.studio.msiha.data.entity.response.OrderActiveResponse
import hi.studio.msiha.data.entity.response.User
import hi.studio.msiha.data.source.local.contentprovider.USER
import hi.studio.hipechat.util.Status
import hi.studio.msiha.data.source.local.contentprovider.DEVICE_TOKEN
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class DashboardFragment : Fragment() {

    private lateinit var dashboardAdapter: DashboardAdapter
    lateinit var user: User
    private val viewModel: DashboardViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        if (menu is MenuBuilder) {
            menu.setOptionalIconsVisible(true)
        }
//        inflater.inflate(R.menu.dashboard_menu, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClick(view)
        observeLiveData()
        viewModel.getAllRequestOut()
        user = Hawk.get(USER)
        Log.d("TAG55", Hawk.get(DEVICE_TOKEN))
    }

    override fun onResume() {
        super.onResume()
        observeLiveData()
    }
    private fun onClick(view: View){
        view.fab_add_dashboard.setOnClickListener {
            findNavController().navigate(R.id.addRequestFragment)
        }
    }

    private fun handleDashboardList(orderList2: ArrayList<OrderActive>){
        dashboardAdapter = DashboardAdapter(orderList2, context!!)

        recyclerViewDashboard.apply {
            adapter = dashboardAdapter
            layoutManager = LinearLayoutManager(activity) as RecyclerView.LayoutManager?
        }

    }

    private fun observeLiveData(){
        viewModel.allRequestResultOut.observe(viewLifecycleOwner, Observer<Resource<OrderActiveResponse>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        progressBarDashboard.visibility =View.INVISIBLE
                        handleDashboardList(it.data.data)
                    } else {

                    }
                }
                Status.ERROR -> {
                    textEmptyDashboard.visibility = View.VISIBLE
                    progressBarDashboard.visibility=View.GONE
                }
                Status.LOADING -> {
                }
            }
        })
    }

}
