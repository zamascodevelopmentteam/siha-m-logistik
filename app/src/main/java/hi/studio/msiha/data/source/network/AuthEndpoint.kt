/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.msiha.data.source.network


/**
 * Created by mochadwi on 2019-11-04
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

const val AUTH_ENDPOINT = "auth"
const val AUTH_REFRESHER_ENDPOINT = "$AUTH_ENDPOINT/refresh"

interface AuthEndpoint {



}