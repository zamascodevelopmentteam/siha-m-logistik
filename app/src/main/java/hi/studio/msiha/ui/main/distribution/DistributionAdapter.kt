package hi.studio.msiha.ui.main.distribution

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.DistributionItems
import hi.studio.msiha.data.source.local.contentprovider.*
import kotlinx.android.synthetic.main.list_item_distribution.view.*
import java.text.SimpleDateFormat
import java.util.*

interface OnDetailClick {
    fun onClick(orderIn: DistributionItems)
}

class DistributionAdapter(
    private var distItem: List<DistributionItems>,
    private var action: OnDetailClick
) : RecyclerView.Adapter<DistributionAdapter.MainViewHolder>(), Filterable {

    private var filterDistItem = distItem


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_distribution, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return filterDistItem.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val currentDist = filterDistItem[position]

        holder.codeName.text = currentDist.droppingNumber
        holder.intendedTo.text = currentDist.recieverName


        var accStatus = currentDist.planStatus
        when (accStatus) {
            EXECUTED -> accStatus = "Diterima"
            DRAFT -> accStatus = "Draft"
            PROCESSED -> accStatus = "Dalam Proses"
            EVALUATION -> accStatus = "Dalam Evaluasi"
            ACCEPTED_FULLY -> accStatus = "Diterima Penuh"
            ACCEPTED_DIFFERENT -> accStatus = "Diterima Sebagian"
            LOST -> accStatus = "Hilang"
            REJECTED -> accStatus = "Ditolah"
        }

        if (currentDist.updatedAt != null) {
            val format2 = "yyyy-MM-dd"
            val localeID = Locale("in", "ID")
            val simpleDateFormat2 = SimpleDateFormat(format2, localeID)
            val date = simpleDateFormat2.parse(currentDist.updatedAt)
            holder.lastUpdate.text = simpleDateFormat2.format(date)
        }

        holder.status.text = "Status : $accStatus"
        holder.wrapper.setOnClickListener {
            action.onClick(currentDist)
        }

    }

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val codeName = view.tv_no_inv_dist
        val intendedTo = view.tv_intended_dist
        val lastUpdate = view.tv_last_update_dist
        val status = view.tv_status_dist
        val wrapper = view.wrapperListDistribution
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            @SuppressLint("LogNotTimber")
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val charSearch: String = p0.toString()
                if (charSearch.isEmpty()) {
                    filterDistItem = distItem
                } else {
                    val resultList = ArrayList<DistributionItems>()
                    for (row in distItem) {
                        if(row.droppingNumber!!.toLowerCase().contains(charSearch.toLowerCase())) resultList.add(row)
                    }
                    filterDistItem = resultList
                }
                val filterResult = FilterResults()
                filterResult.values = filterDistItem
                return filterResult
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                filterDistItem = p1?.values as List<DistributionItems>
                notifyDataSetChanged()
            }
        }
    }
}