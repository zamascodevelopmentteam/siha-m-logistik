package hi.studio.msiha.domain.interactor

import android.content.Context
import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.msiha.data.entity.response.DistributionResponse
import hi.studio.msiha.domain.repository.DistributionRepository
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.toSingleEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class GetDistribution(private val orderRepository: DistributionRepository, val context: Context) :
    CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.IO

    val allDistributionResult = MutableLiveData<Resource<DistributionResponse>>().toSingleEvent()
    val getAllDetailRequestDist = MutableLiveData<Resource<AcceptanceDetailResponse>>().toSingleEvent()


    fun getDistributionDetail(id: String){
        val response = orderRepository.getDetailRequest(id)
        getAllDetailRequestDist.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if(data.isSuccessful){
                val orders = data.body()
                getAllDetailRequestDist.postValue(Resource.success(orders))
            }else{
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    getAllDetailRequestDist.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    getAllDetailRequestDist.postValue(Resource.error("Internal Server Error", null))
                }
            }
        }
    }

    fun getAllOrderActive() {
        val response = orderRepository.getAllDistribution()
        allDistributionResult.postValue(Resource.loading(null))
        launch {
            val data = response.await()
            if (!data.isSuccessful) {
                try {
                    val errObj = JSONObject(data.errorBody().toString())
                    val msg = errObj.getString("message")
                    allDistributionResult.postValue(Resource.error(msg, null))
                } catch (e: Exception) {
                    allDistributionResult.postValue(Resource.error("Internal Server Error", null))
                }
            } else {
                val rsp = data.body()
                allDistributionResult.postValue(Resource.success(rsp))
            }
        }
    }
}