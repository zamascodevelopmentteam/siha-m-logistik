package hi.studio.msiha.data.source.network

import okhttp3.*
import timber.log.Timber
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


abstract class NetworkConnectionInterceptor : Interceptor {

    abstract fun isInternetAvailable(): Boolean

    abstract fun onInternetUnavailable()

    abstract fun onConnectBackendAPIFailed()


    fun createResponse(statusCode: Int, message: String, chain: Interceptor.Chain): okhttp3.Response {
        return Response.Builder()
            .protocol(Protocol.HTTP_1_1)
            .code(statusCode)
            .message("network error")
            .body(
                ResponseBody.create(
                    MediaType.parse("application/json"),
                    """{"message": "$message"}"""
                )
            )
            .request(chain.request())
            .build()
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        Timber.d("<<< Dipanggil, internet available = ${isInternetAvailable()}")
        val request = chain.request()
        if (!isInternetAvailable()) {
            onInternetUnavailable()
            return createResponse(404, "network error", chain)
        }
        return try {
            chain.proceed(request)
        } catch (e: SocketTimeoutException) {
            onConnectBackendAPIFailed()
            createResponse(404, "connect to API error", chain)
        } catch (e: UnknownHostException) {
            onConnectBackendAPIFailed()
            createResponse(404, "cannot resolve API domain name", chain)
        } catch(e:ConnectException){
            onConnectBackendAPIFailed()
            createResponse(404, "cannot connect", chain)
        }
    }
}