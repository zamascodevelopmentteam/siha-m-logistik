package hi.studio.hipechat.data.source.local.contentprovider

import android.content.*
import android.database.Cursor
import android.database.MatrixCursor
import android.net.Uri
import hi.studio.msiha.data.source.PreferencesHelper
import hi.studio.msiha.data.source.local.contentprovider.PREF_FILE_NAME


class SharedPrefProvider : ContentProvider() {

    private lateinit var sharedPref: SharedPreferences


    override fun onCreate(): Boolean {
        if (context == null) {
            return false
        }
        sharedPref = context!!.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
        return true
    }

    override fun getType(uri: Uri): String? {
        return uri.toString()
    }

    override fun query(uri: Uri, projection: Array<String>?, selection: String?, selectionArgs: Array<String>?, sortOrder: String?): Cursor? {
        val pref = uri.pathSegments[2]
        val c = MatrixCursor(arrayOf(pref))
        val category = uri.pathSegments[1]

        when {
            category.equals("1", ignoreCase = true) -> c.addRow(arrayOf(sharedPref.getString(pref, "")))
            category.equals("2", ignoreCase = true) -> c.addRow(arrayOf(sharedPref.getInt(pref, 0)))
            category.equals("3", ignoreCase = true) -> {
                val `val` = sharedPref.getBoolean(pref, false)
                c.addRow(arrayOf(if (`val`) 1 else 0))
            }
        }
        c.setNotificationUri(context!!.contentResolver, uri)
        return c
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val pref = uri.pathSegments[2]
        val category = uri.pathSegments[1]
        val editor = sharedPref.edit()
        when {
            category.equals("1", ignoreCase = true) -> editor.putString(pref, values!!.getAsString(pref))
            category.equals("2", ignoreCase = true) -> editor.putInt(pref, values!!.getAsInteger(pref))
            category.equals("3", ignoreCase = true) -> editor.putBoolean(pref, values!!.getAsBoolean(pref))
        }
        editor.apply()
        val _uri = ContentUris.withAppendedId(CONTENT_URI, 1)
        context!!.contentResolver.notifyChange(_uri, null)
        return _uri
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        val editor = sharedPref.edit()
        editor.clear()
        editor.apply()
        val _uri = ContentUris.withAppendedId(CONTENT_URI, 1)
        context!!.contentResolver.notifyChange(_uri, null)
        return 1
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        insert(uri, values)
        return 1
    }

    companion object {
        private val CONTENT_URI = PreferencesHelper.CONTENT_URI
    }
}
