package hi.studio.msiha.ui.main.request.detailorder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.data.entity.DistributionPlanItems
import kotlinx.android.synthetic.main.list_distribution_detail.view.*

class DetailDistributionAdapter(val items: List<DistributionPlanItems>) :
    RecyclerView.Adapter<DetailDistributionAdapter.MainViewHolder>() {

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var textMedName = view.textMedNameDist1
        var textJumlahBarang = view.textJumlahBarangDistDetail
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_distribution_detail, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val current = items[position]
        holder.textMedName.text = current.medicine?.codeName
        holder.textJumlahBarang.text =
            "Jumlah Barang : " + current.packageQuantity.toString() + " " + current.packageUnitType
    }
}