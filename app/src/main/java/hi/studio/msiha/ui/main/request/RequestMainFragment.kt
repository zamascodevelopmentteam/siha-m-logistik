package hi.studio.msiha.ui.main.request


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.tabs.TabLayout

import hi.studio.msiha.R
import kotlinx.android.synthetic.main.fragment_request_main.*

/**
 * A simple [Fragment] subclass.
 */
class RequestMainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_request_main, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        createdViewPager()
    }

    private fun createdViewPager() {
        val requestListMainAdapter = RequestMainAdapter(childFragmentManager)
        request_viewpager.adapter = requestListMainAdapter
        request_viewpager.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(request_main_tab)
        )
        request_main_tab.setupWithViewPager(request_viewpager)

        request_main_tab.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                request_viewpager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }
}
