/*
 * Copyright (c) 2019.
 * PT. HIPE INDONESIA
 */

package hi.studio.hipechat.util.extension

import android.view.View


/**
 * Created by mochadwi on 2019-10-23
 * Copyright (c) 2019 Hipe.id. All rights reserved.
 */

fun View.gone() {
    visibility = View.GONE
}

fun View.visible() {
    visibility = View.VISIBLE
}
