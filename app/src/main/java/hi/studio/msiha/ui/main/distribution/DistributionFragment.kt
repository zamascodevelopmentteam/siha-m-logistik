package hi.studio.msiha.ui.main.distribution


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.DistributionItems
import hi.studio.msiha.data.entity.response.DistributionResponse
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.fragment_distribution.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DistributionFragment : Fragment(), SearchView.OnQueryTextListener, OnDetailClick {

    private var distAdapter: DistributionAdapter = DistributionAdapter(ArrayList(), this@DistributionFragment)
    private val viewModel: DistributionViewModel by viewModel()
    private lateinit var menuItemSearch: MenuItem


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_distribution, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()
        viewModel.getAvailMed()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_search, menu)
        menuItemSearch = menu.findItem(R.id.menuSearch)
//        menuItemSearch.setVisible(false)
        val searchView: SearchView = menuItemSearch.actionView as SearchView
        searchView.setOnQueryTextListener(this)
    }

    override fun onResume() {
        super.onResume()
        observeLiveData()
    }

    private fun handleDistributionList(distList: List<DistributionItems>) {
        distAdapter = DistributionAdapter(distList, this@DistributionFragment)

        rv_dist.apply {
            adapter = distAdapter
            layoutManager = LinearLayoutManager(activity)
        }
    }

    private fun observeLiveData() {
        viewModel.distributionResult.observe(this, Observer<Resource<DistributionResponse>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
//                        menuItemSearch.setVisible(true)
                        progressBarDist.visibility = View.INVISIBLE
                        handleDistributionList(it.data.data)
                    } else {
//                        menuItemSearch.setVisible(false)
                        progressBarDist.visibility = View.GONE
                        textEmptyDist.visibility = View.VISIBLE
                    }
                }
                Status.ERROR -> {
//                    menuItemSearch.setVisible(false)
                    progressBarDist.visibility = View.GONE
                    textEmptyDist.visibility = View.VISIBLE
//                    Toast.makeText(context, "Tidak ada ", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> {
                }
            }
        })
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        distAdapter.filter.filter(query.toString())
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        distAdapter.filter.filter(newText.toString())
        return false
    }

    override fun onClick(orderIn: DistributionItems) {
        val bundle = Bundle()
        bundle.putString("id", orderIn.id.toString())
        findNavController().navigate(R.id.detailDistributionFragment, bundle)
    }


}
