package hi.studio.msiha.data.source.network

interface InternetConnectionListener {
    fun onInternetUnavailable()
    fun onConnectAPIFailed()
}