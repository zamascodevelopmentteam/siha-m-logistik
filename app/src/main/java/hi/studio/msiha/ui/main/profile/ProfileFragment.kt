package hi.studio.msiha.ui.main.profile


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.orhanobut.hawk.Hawk

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.response.User
import hi.studio.msiha.data.source.local.contentprovider.DEVICE_TOKEN
import hi.studio.msiha.data.source.local.contentprovider.TOKEN
import hi.studio.msiha.data.source.local.contentprovider.USER
import hi.studio.msiha.ui.auth.AuthActivity
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

class ProfileFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onBindUser(view)

    }

    private fun onBindUser(view: View) {
        val user: User = Hawk.get(USER) as User

        profileName.text = user.name
        profileRole.text = user.role

        Glide
            .with(this@ProfileFragment)
            .load(user.avatar)
            .placeholder(R.mipmap.ic_avatar)
            .into(view.profilePic)

        btLogout.setOnClickListener {
            Log.d("TAG55", Hawk.get(TOKEN))

            Hawk.deleteAll()
            activity?.finish()
            startActivity(Intent(activity, AuthActivity::class.java))
        }
    }


}
