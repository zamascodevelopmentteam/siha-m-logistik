package hi.studio.msiha.ui.main.distribution.Detail


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import hi.studio.msiha.R
import hi.studio.msiha.data.entity.DetailAcceptance
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.msiha.data.source.local.contentprovider.*
import hi.studio.msiha.ui.main.distribution.DistributionViewModel
import hi.studio.msiha.ui.main.request.detailorder.DetailDistributionAdapter
import hi.studio.hipechat.util.Resource
import hi.studio.hipechat.util.Status
import kotlinx.android.synthetic.main.fragment_detail_distribution.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class DetailDistributionFragment : Fragment() {

    private val viewModel: DistributionViewModel by viewModel()
    private lateinit var detailListAdapter: DetailDistributionAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_distribution, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProgressBar()
        val id = arguments?.getString("id").toString()
        viewModel.getRequestDetail(id)
        observeLiveData()

    }

    override fun onResume() {
        super.onResume()
        observeLiveData()
    }

    private fun bindItem(distDetail: DetailAcceptance){
        val format2 = "yyyy-MM-dd"
        val localeID = Locale("in", "ID")
        val simpleDateFormat2 = SimpleDateFormat(format2, localeID)
        if(distDetail.createdAt != null){
            val make = simpleDateFormat2.parse(distDetail.createdAt)
            tv_date_make_detailDist.text = simpleDateFormat2.format(make)

        }
        if(distDetail.updatedAt != null){
            val edit = simpleDateFormat2.parse(distDetail.updatedAt)
            tv_date_edit_detailDist.text = simpleDateFormat2.format(edit)
        }

        var approvalStatus = distDetail.approvalStatus
        when (approvalStatus) {
            IN_EVALUATION -> approvalStatus = "Review"
            CONFIRMED -> approvalStatus = "Sudah Disetujui"
            APPROVED_FULLY -> approvalStatus = "Buat Usulan Kebutuhan"
            APPROVED_FULL -> approvalStatus = "Buat Usulan Kebutuhan"
            REJECTED_INTERNAL -> approvalStatus = "Tidak Disetujui"
            REJECTED_UPPER -> approvalStatus = "Tidak Membuat Usulan Kebutuhan"
            DRAFT -> approvalStatus = "Draft"
        }

        var accStatus = distDetail.planStatus
        when (accStatus) {
            EXECUTED -> accStatus = "Diterima"
            DRAFT -> accStatus = "Draft"
            PROCESSED -> accStatus = "Dalam Proses"
            EVALUATION -> accStatus = "Dalam Evaluasi"
            ACCEPTED_FULLY -> accStatus = "Diterima Penuh"
            ACCEPTED_DIFFERENT -> accStatus = "Diterima Sebagian"
            LOST -> accStatus = "Hilang"
            REJECTED -> accStatus = "Ditolah"
        }

        tv_surat_jalan_detail.text = distDetail.droppingNumber
        tv_status_pengiriman_detailDist.text = accStatus
        tv_status_apr_detailDist.text = approvalStatus
        tv_note_apr_detailDist.text = distDetail.approvalNotes
        tv_note_acc_detailDist.text = distDetail.notes
//        tv_make_by_detailDist.text = distDetail?.planStatus
        detailListAdapter = DetailDistributionAdapter(distDetail.distributionPlanItems)
        rv_order_dist1.apply {
            adapter = detailListAdapter
            layoutManager = LinearLayoutManager(activity)
        }

    }

    private fun showProgressBar() {
        progressBarDetailDistribution.visibility = View.VISIBLE
        wrapperDetailOrderDist.visibility = View.GONE
    }

    private fun hideProgressBar() {
        progressBarDetailDistribution.visibility = View.GONE
        wrapperDetailOrderDist.visibility = View.VISIBLE
    }

    private fun observeLiveData(){
        viewModel.getDetailResult.observe(this, Observer<Resource<AcceptanceDetailResponse>?> {
            when (it?.status) {
                Status.SUCCESS -> {
                    if (it.data?.data != null) {
                        bindItem(it.data.data)
                        hideProgressBar()
                    } else {
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> {
                }
            }
        })
    }

}
