package hi.studio.msiha.ui.main.distribution

import androidx.lifecycle.ViewModel
import hi.studio.msiha.data.entity.response.AcceptanceDetailResponse
import hi.studio.msiha.data.entity.response.DistributionResponse
import hi.studio.msiha.domain.interactor.GetDistribution
import hi.studio.hipechat.util.LiveEvent
import hi.studio.hipechat.util.Resource

class DistributionViewModel(private val getOrder: GetDistribution): ViewModel() {

    val distributionResult: LiveEvent<Resource<DistributionResponse>>
        get() = getOrder.allDistributionResult

    fun getAvailMed(){
        getOrder.getAllOrderActive()
    }

    fun getRequestDetail(id: String){
        getOrder.getDistributionDetail(id)
    }

    val getDetailResult: LiveEvent<Resource<AcceptanceDetailResponse>>
        get() = getOrder.getAllDetailRequestDist
}