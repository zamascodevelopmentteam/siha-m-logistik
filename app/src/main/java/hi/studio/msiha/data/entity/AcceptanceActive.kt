package hi.studio.msiha.data.entity

data class AcceptanceActive(
    val id: String,
    var droppingNumber : Int? = 0,
    val orderId: Int? = 0,
    val picId  : Int? = 0,
    var planStatus : String? = null,
    var entityName : String? = null
) {
    companion object{
        fun empty() = AcceptanceActive(
            "",
            0,
            0,
            0,
            "",
            ""
        )
    }
}