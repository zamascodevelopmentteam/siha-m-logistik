package hi.studio.msiha.data.entity.response

import hi.studio.msiha.data.entity.OrderActive

data class OrderActiveResponse(
    val data: ArrayList<OrderActive>
)