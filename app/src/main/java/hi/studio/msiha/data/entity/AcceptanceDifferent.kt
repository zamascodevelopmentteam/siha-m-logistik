package hi.studio.msiha.data.entity

data class AcceptanceDifferent(
    private val id: Int?,
    private val actualReceivedPackageQuantity: Int?
)